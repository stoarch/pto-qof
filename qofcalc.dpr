program qofcalc;

uses
  Forms,
  form_main in 'src\form_main.pas' {PtoMainForm},
  frame_coeff_editor in 'src\frame_coeff_editor.pas' {CoefficientsFrame: TFrame},
  datamodule_main in 'src\datamodule_main.pas' {DataModuleMain: TDataModule},
  frame_elsch_view in 'src\frame_elsch_view.pas' {ElschViewFrame: TFrame},
  frame_gss_view in 'src\frame_gss_view.pas' {GSSViewerFrame: TFrame},
  meter_value_list in 'src\meter_value_list.pas',
  data_meterValue in 'src\data_meterValue.pas',
  factory_Meter in 'src\factory_Meter.pas',
  form_meterGraphViewer in 'src\form_meterGraphViewer.pas' {MeterGraphViewerForm},
  frame_qotCalculator in 'src\frame_qotCalculator.pas' {QotCalculatorFrame: TFrame},
  frame_massflowrate_view in 'src\frame_massflowrate_view.pas' {WaterFlowrateFrame: TFrame},
  const_archives in 'src\const_archives.pas',
  form_progressDisplayer in 'src\form_progressDisplayer.pas' {ProgressDisplayerForm};

{$R *.res}

begin
  Application.Initialize;

  DataModuleMain := TDataModuleMain.Create( Application );

  Application.CreateForm(TPtoMainForm, PtoMainForm);
  Application.Run;
end.
