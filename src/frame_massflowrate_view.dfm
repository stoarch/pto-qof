object WaterFlowrateFrame: TWaterFlowrateFrame
  Left = 0
  Top = 0
  Width = 620
  Height = 420
  TabOrder = 0
  DesignSize = (
    620
    420)
  object sLabel1: TsLabel
    Left = 4
    Top = 4
    Width = 315
    Height = 18
    Caption = #1055#1086#1082#1072#1079#1072#1085#1080#1103' '#1088#1072#1089#1093#1086#1076#1072' '#1074#1086#1076#1099' "'#1057#1077#1074#1077#1088'-'#1070#1075'"'
    ParentFont = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
  end
  object dbnvgr1: TDBNavigator
    Left = 4
    Top = 32
    Width = 215
    Height = 25
    DataSource = DataModuleMain.dsSeverUgArc
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
    TabOrder = 0
  end
  object dbgrd1: TDBGrid
    Left = 4
    Top = 64
    Width = 611
    Height = 354
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataModuleMain.dsSeverUgArc
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object selectChanelComboBox: TsComboBox
    Left = 468
    Top = 36
    Width = 145
    Height = 22
    Anchors = [akTop, akRight]
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = #1050#1072#1085#1072#1083':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'COMBOBOX'
    Style = csDropDownList
    ItemHeight = 16
    ItemIndex = 0
    TabOrder = 2
    Text = #1042#1089#1077
    OnChange = selectChanelComboBoxChange
    Items.Strings = (
      #1042#1089#1077
      #1057#1077#1074#1077#1088
      #1070#1075)
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'GROUPBOX'
    Left = 148
    Top = 108
  end
end
