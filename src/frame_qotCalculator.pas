unit frame_qotCalculator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sFrameAdapter, StdCtrls, sLabel, DB, kbmMemTable, ExtCtrls,
  TeeProcs, TeEngine, Chart, ComCtrls, sPageControl, Buttons, sBitBtn,
  sTooledit, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit, Grids, DBGrids,
  sCheckBox, Series, sGroupBox, DBCtrls;

type
  TQotCalculatorFrame = class(TFrame)
    sFrameAdapter1: TsFrameAdapter;
    sLabel1: TsLabel;
    Calculate: TsBitBtn;
    FunctionPages: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    EvCalculationSheet: TsTabSheet;
    chtEv: TChart;
    dbgrd1: TDBGrid;
    Ctg1Edit: TsCalcEdit;
    Ctg2Edit: TsCalcEdit;
    StartPeriodDateedit: TsDateEdit;
    EndPeriodDateEdit: TsDateEdit;
    K21Edit: TsCalcEdit;
    K38Edit: TsCalcEdit;
    dbgrd2: TDBGrid;
    chtEsn: TChart;
    K52Edit: TsCalcEdit;
    K74Edit: TsCalcEdit;
    K2RAEdit: TsCalcEdit;
    K4RBEdit: TsCalcEdit;
    Ev2VisibleCheckBox: TsCheckBox;
    EVVisibleCheckBox: TsCheckBox;
    Ev1VisibleCheckBox: TsCheckBox;
    Esn21VisibleCheckBox: TsCheckBox;
    Esn38VisibleCheckBox: TsCheckBox;
    Esn52VisibleCheckBox: TsCheckBox;
    Esn74VisibleCheckBox: TsCheckBox;
    Esn2RaVisibleCheckBox: TsCheckBox;
    Esn4RbVisibleCheckBox: TsCheckBox;
    EsnVisibilityCheckBox: TsCheckBox;
    DBGrid1: TDBGrid;
    chtQot: TChart;
    QotVisibilityCheckbox: TsCheckBox;
    QotSevVisibilityCheckBox: TsCheckBox;
    QotUgVisibilityCheckbox: TsCheckBox;
    sGroupBox1: TsGroupBox;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    bte_edit: TsCalcEdit;
    DBGrid2: TDBGrid;
    avgFuelChart: TChart;
    sTabSheet4: TsTabSheet;
    DBGrid3: TDBGrid;
    chtGas: TChart;
    qg_edit: TsCalcEdit;
    sLabel6: TsLabel;
    qpar_edit: TsCalcEdit;
    DBNavigator1: TDBNavigator;
    DBNavigator2: TDBNavigator;
    DBNavigator3: TDBNavigator;
    DBNavigator4: TDBNavigator;
    DBNavigator5: TDBNavigator;
    BeeSeriesVisibleCheckbox: TsCheckBox;
    EvSeriesVisibleCheckbox: TsCheckBox;
    GgSeriesVisibleCheckbox: TsCheckBox;
    QotSeriesVisibleCheckbox: TsCheckBox;
    EsnSeriesVisibleCheckbox: TsCheckBox;
    procedure CalculateClick(Sender: TObject);
    procedure Ev1VisibleCheckBoxClick(Sender: TObject);
    procedure Ev2VisibleCheckBoxClick(Sender: TObject);
    procedure EVVisibleCheckBoxClick(Sender: TObject);
    procedure Esn21VisibleCheckBoxClick(Sender: TObject);
    procedure Esn38VisibleCheckBoxClick(Sender: TObject);
    procedure Esn52VisibleCheckBoxClick(Sender: TObject);
    procedure Esn74VisibleCheckBoxClick(Sender: TObject);
    procedure Esn2RaVisibleCheckBoxClick(Sender: TObject);
    procedure Esn4RbVisibleCheckBoxClick(Sender: TObject);
    procedure EsnVisibilityCheckBoxClick(Sender: TObject);
    procedure FunctionPagesChange(Sender: TObject);
    procedure QotVisibilityCheckboxClick(Sender: TObject);
    procedure QotSevVisibilityCheckBoxClick(Sender: TObject);
    procedure QotUgVisibilityCheckboxClick(Sender: TObject);
    procedure BeeSeriesVisibleCheckboxClick(Sender: TObject);
    procedure EvSeriesVisibleCheckboxClick(Sender: TObject);
    procedure QotSeriesVisibleCheckboxClick(Sender: TObject);
    procedure EsnSeriesVisibleCheckboxClick(Sender: TObject);
    procedure GgSeriesVisibleCheckboxClick(Sender: TObject);
  private
    FGasSeries: TLineSeries;
    FQotSeries: TLineSeries;
    FQsevSeries: TLineSeries;
    FQugSeries: TLineSeries;

    FEvSeries: TLineSeries;
    FEv1Series: TLineSeries;
    FEv2Series: TLineSeries;

    FEsn21Series: TLineSeries;
    FEsn38Series: TLineSeries;
    FEsn52Series: TLineSeries;
    FEsn74Series: TLineSeries;
    FEsn2RaSeries: TLineSeries;
    FEsn4RbSeries: TLineSeries;
    FEsnSeries: TLineSeries;

    FAvgFuelSeries : TLineSeries;
    FQot2Series : TLineSeries;
    FEv3Series : TLineSeries;
    FEsn2Series : TLineSeries;
    FGgSeries : TLineSeries;

    procedure CalculateValues;
    procedure CalculateElectricity;
    procedure DisplayElectricityGraph;
    procedure LoadGeneratorValues;
    procedure CalculateEsn;
    procedure DisplayEsnGraph;
    procedure LoadEsnValues;
    procedure CalculateHeatDelivery;
    procedure DisplayHeatDeliveryGraph;
    procedure LoadHeatDeliveryValues;
    function MakeSeries(ATitle: string): TLineSeries;
    procedure UpdateCalculateCaption;
    procedure CalculateStationEfficiency;
    procedure CalculateAverageFuelConsumption;
    procedure DisplayAvgFuelConsGraph;
    procedure LoadAvgFuelConsumption;
    procedure CalculateGasConsumption;
    procedure DisplayGasGraph;
    procedure LoadGasValues;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);override;
  end;

implementation

uses
  datamodule_main, form_progressDisplayer;

{$R *.dfm}

{ TQotCalculatorFrame }

constructor TQotCalculatorFrame.Create(AOwner: TComponent);
begin
  inherited;

    StartPeriodDateedit.Date := Now - 7;
    EndPeriodDateEdit.Date := Now;

    FunctionPages.ActivePageIndex := 0;
    UpdateCalculateCaption();
end;

procedure TQotCalculatorFrame.CalculateClick(Sender: TObject);
begin
    CalculateValues();
end;

procedure TQotCalculatorFrame.CalculateValues();
    const
        GAS_CALCULATE = 4; //gas consumption
        EV_CALCULATE  = 3;  //electricity production
        ESN_CALCULATE = 2; //self usage electricity
        QOT_CALCULATE = 1; //heat dissipation
        BEE_CALCULATE = 0; //station efficience
begin
    case FunctionPages.ActivePageIndex of
        GAS_CALCULATE: CalculateGasConsumption();
        ESN_CALCULATE: CalculateEsn();
        EV_CALCULATE: CalculateElectricity();
        QOT_CALCULATE: CalculateHeatDelivery();
        BEE_CALCULATE: CalculateStationEfficiency();
    end;
end;

procedure TQotCalculatorFrame.CalculateGasConsumption();
begin
    LoadGasValues();
    DisplayGasGraph();
end;

procedure TQotCalculatorFrame.LoadGasValues();
begin
    DataModuleMain.SetDatePeriod(
        StartPeriodDateEdit.Date,
        EndPeriodDateEdit.Date
    );

    DataModuleMain.CalculateGasConsumption( qg_edit.Value );
end;

procedure TQotCalculatorFrame.DisplayGasGraph();
begin
    if( assigned(FGasSeries) )then
        FGasSeries.Free();

    FGasSeries := MakeSeries('Gg');

    with DataModuleMain, kbmGas do
    begin
        DisableControls();

        First();

        while not Eof do
        begin
            FGasSeries.AddXY( kbmGasDateCalc.Value, kbmGasGg.Value );

            Next();
        end;

        EnableControls();
    end;

    chtGas.AddSeries( FGasSeries );
end;

procedure TQotCalculatorFrame.CalculateStationEfficiency();
    var
        progress : TProgressDisplayerForm;
begin
    progress := TProgressDisplayerForm.Create(nil);
    try
        progress.WorkMessage := '���������� �������������. ��������� ����������...';
        progress.MaxValue := 5;
        progress.progress := 0;
        progress.Show();

        progress.SubMessage := '������ ��������� ��. �������';

        CalculateElectricity();

        progress.progress := 1;
        progress.SubMessage := '������ ��. ������� �� ����������� �����';

        CalculateEsn();

        progress.progress := 2;
        progress.SubMessage := '������ ������� �����';

        CalculateHeatDelivery();

        progress.progress := 3;
        progress.SubMessage := '������ ������� ����';

        CalculateGasConsumption();

        progress.progress := 4;
        progress.SubMessage := '������ �������� �������� �������';

        CalculateAverageFuelConsumption();

    finally
        progress.Free();
    end;
end;

procedure TQotCalculatorFrame.CalculateAverageFuelConsumption();
begin
    LoadAvgFuelConsumption();
    DisplayAvgFuelConsGraph();
end;

procedure TQotCalculatorFrame.LoadAvgFuelConsumption();
begin
    DataModuleMain.SetDatePeriod(
        StartPeriodDateEdit.Date,
        EndPeriodDateEdit.Date
    );

    DataModuleMain.CalculateAvgFuelConsumption(bte_edit.value, qpar_edit.value);
end;

procedure TQotCalculatorFrame.DisplayAvgFuelConsGraph();
begin
    if( assigned(FAvgFuelSeries) )then
        FAvgFuelSeries.Free();
    if( assigned(FQot2Series) )then
        FQot2Series.Free();
    if( assigned(FEv3Series) )then
        FEv3Series.Free();
    if( assigned(FEsn2Series) )then
        FEsn2Series.Free();
    if( assigned(FGgSeries) )then
        FGgSeries.Free();

    FAvgFuelSeries := MakeSeries('AvgFuel');
    FQot2Series := MakeSeries('Qot');
    FEv3Series := MakeSeries('Ev');
    FEsn2Series := MakeSeries('Esn');
    FGgSeries := MakeSeries('Gg');

    with DataModuleMain, kbmAvgFuelCons do
    begin
        DisableControls();

        First();

        while not Eof do
        begin
            FAvgFuelSeries.AddXY( kbmAvgFuelConsDateCalc.Value, kbmAvgFuelConsbee.Value );
            FQot2Series.AddXY( kbmAvgFuelConsDateCalc.Value, kbmAvgFuelConsqot.Value );
            FEv3Series.AddXY( kbmAvgFuelConsDateCalc.Value, kbmAvgFuelConsev.Value );
            FEsn2Series.AddXY( kbmAvgFuelConsDateCalc.Value, kbmAvgFuelConsesn.Value );
            FGgSeries.AddXY( kbmAvgFuelConsDateCalc.Value, kbmAvgFuelConsGg.Value );

            Next();
        end;

        EnableControls();
    end;

    avgFuelChart.AddSeries( FAvgFuelSeries );
    avgFuelChart.AddSeries( FQot2Series );
    avgFuelChart.AddSeries( FEv3Series );
    avgFuelChart.AddSeries( FEsn2Series );
    avgFuelChart.AddSeries( FGgSeries );
end;


procedure TQotCalculatorFrame.CalculateHeatDelivery();
begin
  LoadHeatDeliveryValues();
  DisplayHeatDeliveryGraph();
end;

procedure TQotCalculatorFrame.LoadHeatDeliveryValues();
begin
    DataModuleMain.SetDatePeriod(
        StartPeriodDateEdit.Date,
        EndPeriodDateEdit.Date
    );

    DataModuleMain.CalculateQot();
end;

procedure TQotCalculatorFrame.DisplayHeatDeliveryGraph();
begin
    if( assigned(FQotSeries) )then
        FQotSeries.Free();
    if( assigned(FQsevSeries) )then
        FQsevSeries.Free();
    if( assigned(FQugSeries) )then
        FQugSeries.Free();

    FQotSeries := MakeSeries('Qot');
    FQsevSeries := MakeSeries('Qsev');
    FQugSeries := MakeSeries('Qug');

    with DataModuleMain, kbmQot do
    begin
        DisableControls();

        First();

        while not Eof do
        begin
            FQotSeries.AddXY( kbmQotDateArc.Value, kbmQotQot.Value );
            FQsevSeries.AddXY( kbmQotDateArc.Value, kbmQotQsev.Value );
            FQugSeries.AddXY( kbmQotDateArc.Value, kbmQotQug.Value );

            Next();
        end;

        EnableControls();
    end;

    chtQot.AddSeries( FQotSeries );
    chtQot.AddSeries( FQsevSeries );
    chtQot.AddSeries( FQugSeries );

end;


procedure TQotCalculatorFrame.CalculateElectricity();
begin
    LoadGeneratorValues();
    DisplayElectricityGraph();
end;

procedure TQotCalculatorFrame.CalculateEsn();
begin
    LoadEsnValues();
    DisplayEsnGraph();
end;

procedure TQotCalculatorFrame.LoadEsnValues();
begin
    DataModuleMain.SetDatePeriod(
        StartPeriodDateEdit.Date,
        EndPeriodDateEdit.Date
    );

    DataModuleMain.CalculateEsn( K21Edit.Value, K38Edit.Value, K52Edit.Value, K74Edit.Value, K2RAEdit.Value, K4RBEdit.Value );
end;

function TQotCalculatorFrame.MakeSeries( ATitle: string ): TLineSeries;
begin
    Result := TLineSeries.Create(self);
    Result.XValues.DateTime := true;
    Result.Title := ATitle;
    Result.ShowInLegend := Atitle <> '';
end;

procedure TQotCalculatorFrame.DisplayEsnGraph();
begin
    chtEsn.SeriesList.Clear();

    if( Assigned(FEsn21Series) )then
        FEsn21Series.Free();
    if( Assigned(FEsn38Series) )then
        FEsn38Series.Free();
    if( Assigned(FEsn52Series) )then
        FEsn52Series.Free();
    if( Assigned(FEsn74Series) )then
        FEsn74Series.Free();
    if( Assigned(FEsn2RaSeries) )then
        FEsn2RaSeries.Free();
    if( Assigned(FEsn4RbSeries) )then
        FEsn4RbSeries.Free();
    if( Assigned(FEsnSeries) )then
        FEsnSeries.Free();

    FEsn21Series := MakeSeries('21');
    FEsn38Series := MakeSeries('38');
    FEsn52Series := MakeSeries('52');
    FEsn74Series := MakeSeries('74');
    FEsn2RaSeries := MakeSeries('2Pa');
    FEsn4RbSeries := MakeSeries('4Pb');
    FEsnSeries := MakeSeries('Esn');

    with DataModuleMain,kbmEsn do
    begin
        DisableControls();

        First();

        while not Eof do
        begin
            FEsn21Series.AddXY( kbmEsnCurTime.Value, kbmEsnEsn21.Value );
            FEsn38Series.AddXY( kbmEsnCurTime.Value, kbmEsnEsn38.Value );
            FEsn52Series.AddXY( kbmEsnCurTime.Value, kbmEsnEsn52.Value );
            FEsn74Series.AddXY( kbmEsnCurTime.Value, kbmEsnEsn74.Value );
            FEsn2RaSeries.AddXY( kbmEsnCurTime.Value, kbmEsnEsn2Ra.Value );
            FEsn4RbSeries.AddXY( kbmEsnCurTime.Value, kbmEsnEsn4Rb.Value );

            FEsnSeries.AddXY( kbmEsnCurTime.Value, kbmEsnEsn.Value );

            Next();
        end;

        First();
        EnableControls();
    end;

    chtEsn.AddSeries( FEsn21Series );
    chtEsn.AddSeries( FEsn38Series );
    chtEsn.AddSeries( FEsn52Series );
    chtEsn.AddSeries( FEsn74Series );
    chtEsn.AddSeries( FEsn2RaSeries );
    chtEsn.AddSeries( FEsn4RbSeries );
    chtEsn.AddSeries( FEsnSeries );

end;

procedure TQotCalculatorFrame.LoadGeneratorValues();
begin
    DataModuleMain.SetDatePeriod(
        StartPeriodDateEdit.Date,
        EndPeriodDateEdit.Date
    );

    DataModuleMain.CalculateEv( Ctg1Edit.Value, Ctg2Edit.Value );
end;

procedure TQotCalculatorFrame.DisplayElectricityGraph();
begin
    chtEv.SeriesList.Clear();

    if( Assigned(FEvSeries) )then
        FEvSeries.Free();
    if( Assigned(FEv1Series) )then
        FEv1Series.Free();
    if( Assigned(FEv2Series) )then
        FEv2Series.Free();

    FEvSeries := TLineSeries.Create(self);
    FEvSeries.XValues.DateTime := true;
    FEvSeries.LinePen.Color := clRed;
    FEvSeries.LinePen.Style := psSolid;

    FEv1Series := TLineSeries.Create(self);
    FEv1Series.XValues.DateTime := true;
    FEv1Series.LinePen.Color := clBlue;
    FEv1Series.LinePen.Style := psSolid;

    FEv2Series := TLineSeries.Create(self);
    FEv2Series.XValues.DateTime := true;
    FEv2Series.LinePen.Color := clGreen;
    FEv2Series.LinePen.Style := psSolid;

    with DataModuleMain,kbmEvTable do
    begin
        DisableControls();

        First();

        while not Eof do
        begin
            FEvSeries.AddXY( kbmEvTableCurTime.Value, kbmEvTableEv.Value );
            FEv1Series.AddXY( kbmEvTableCurTime.Value, kbmEvTableEv1.Value );
            FEv2Series.AddXY( kbmEvTableCurTime.Value, kbmEvTableEv2.Value );

            Next();
        end;

        First();
        EnableControls();
    end;

    chtEv.AddSeries( FEvSeries );
    chtEv.AddSeries( FEv1Series );
    chtEv.AddSeries( FEv2Series );

end;



procedure TQotCalculatorFrame.Ev1VisibleCheckBoxClick(Sender: TObject);
begin
    if(not Assigned(FEv1Series))then
        exit;

    FEv1Series.Active := Ev1VisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Ev2VisibleCheckBoxClick(Sender: TObject);
begin
    if(not Assigned(FEv2Series))then
        exit;

    FEv2Series.Active := Ev2VisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.EVVisibleCheckBoxClick(Sender: TObject);
begin
    if(not Assigned(FEvSeries))then
        exit;

    FEvSeries.Active := EvVisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Esn21VisibleCheckBoxClick(Sender: TObject);
begin
    FEsn21Series.Active := Esn21VisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Esn38VisibleCheckBoxClick(Sender: TObject);
begin
    FEsn38Series.Active := Esn38VisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Esn52VisibleCheckBoxClick(Sender: TObject);
begin
    FEsn52Series.Active := Esn52VisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Esn74VisibleCheckBoxClick(Sender: TObject);
begin
    FEsn74Series.Active := Esn74VisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Esn2RaVisibleCheckBoxClick(Sender: TObject);
begin
    FEsn2RaSeries.Active := Esn2RaVisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.Esn4RbVisibleCheckBoxClick(Sender: TObject);
begin
    FEsn4RbSeries.Active := Esn4RbVisibleCheckBox.Checked;
end;

procedure TQotCalculatorFrame.EsnVisibilityCheckBoxClick(Sender: TObject);
begin
    if( not assigned(FEsnSeries))then
        exit;

    FEsnSeries.Active := EsnVisibilityCheckbox.Checked;
end;

procedure TQotCalculatorFrame.FunctionPagesChange(Sender: TObject);
begin
  UpdateCalculateCaption();
end;

procedure TQotCalculatorFrame.UpdateCalculateCaption();
    const
        EFFECTIVITY_PAGE = 0;
        HEAT_DISS_PAGE = 1;
        EL_SELF_PAGE = 2;
        EL_PROD_PAGE = 3;
        GAS_PAGE = 4;
begin
  case FunctionPages.ActivePageIndex of
    EFFECTIVITY_PAGE :  Calculate.Caption := '��������� b��';
    HEAT_DISS_PAGE :  Calculate.Caption := '��������� Qot';
    EL_SELF_PAGE :  Calculate.Caption := '��������� ���';
    EL_PROD_PAGE :  Calculate.Caption := '��������� ��';
    GAS_PAGE : Calculate.Caption := '��������� G�';
  end;
end;

procedure TQotCalculatorFrame.QotVisibilityCheckboxClick(Sender: TObject);
begin
    FQotSeries.Active := QotVisibilityCheckbox.Checked;
end;

procedure TQotCalculatorFrame.QotSevVisibilityCheckBoxClick(
  Sender: TObject);
begin
    FQsevSeries.Active := QotSevVisibilityCheckBox.Checked;
end;

procedure TQotCalculatorFrame.QotUgVisibilityCheckboxClick(
  Sender: TObject);
begin
    FQugSeries.Active := QotUgVisibilityCheckbox.Checked;
end;

procedure TQotCalculatorFrame.BeeSeriesVisibleCheckboxClick(
  Sender: TObject);
begin
    FAvgFuelSeries.Active := BeeSeriesVisibleCheckbox.Checked;
end;

procedure TQotCalculatorFrame.EvSeriesVisibleCheckboxClick(
  Sender: TObject);
begin
    FEv3Series.Active := EvSeriesVisibleCheckbox.Checked;
end;

procedure TQotCalculatorFrame.QotSeriesVisibleCheckboxClick(
  Sender: TObject);
begin
    FQot2Series.Active := QotSeriesVisibleCheckbox.Checked;
end;

procedure TQotCalculatorFrame.EsnSeriesVisibleCheckboxClick(
  Sender: TObject);
begin
    FEsn2Series.Active := EsnSeriesVisibleCheckbox.Checked;
end;

procedure TQotCalculatorFrame.GgSeriesVisibleCheckboxClick(
  Sender: TObject);
begin
    FGgSeries.Active := GgSeriesVisibleCheckbox.Checked;
end;

end.
