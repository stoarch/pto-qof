unit factory_Meter;

interface

uses
  data_meterValue, meter_value_list, classes, form_meterGraphViewer;

type
  MeterFactory = class
  public
    class function MakeGraphViewer( AOwner: TComponent ) : TMeterGraphViewerForm;
    class function MakeValue() : TMeterValue;
    class function MakeValueList() : TMeterValueList;
  end;

implementation

{ MeterFactory }

class function MeterFactory.MakeGraphViewer(
  AOwner: TComponent): TMeterGraphViewerForm;
begin
    Result := TMeterGraphViewerForm.Create( AOwner );
end;

class function MeterFactory.MakeValue: TMeterValue;
begin
    Result := TMeterValue.Create();
end;

class function MeterFactory.MakeValueList: TMeterValueList;
begin
    Result := TMeterValueList.Create();
end;

end.
 