object QotCalculatorFrame: TQotCalculatorFrame
  Left = 0
  Top = 0
  Width = 1009
  Height = 607
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Lucida Sans Unicode'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  DesignSize = (
    1009
    607)
  object sLabel1: TsLabel
    Left = 4
    Top = 8
    Width = 338
    Height = 23
    Caption = #1056#1072#1089#1095#1105#1090' '#1091#1076#1077#1083#1100#1085#1086#1075#1086' '#1088#1072#1089#1093#1086#1076#1072' '#1090#1086#1087#1083#1080#1074#1072
    ParentFont = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Lucida Sans Unicode'
    Font.Style = []
  end
  object Calculate: TsBitBtn
    Left = 4
    Top = 36
    Width = 149
    Height = 49
    Caption = #1056#1072#1089#1089#1095#1080#1090#1072#1090#1100
    TabOrder = 0
    OnClick = CalculateClick
    SkinData.SkinSection = 'BUTTON'
  end
  object FunctionPages: TsPageControl
    Left = 4
    Top = 156
    Width = 1001
    Height = 450
    ActivePage = sTabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 3
    OnChange = FunctionPagesChange
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = 'b'#1101#1101
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      DesignSize = (
        993
        415)
      object bte_edit: TsCalcEdit
        Left = 4
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = 'b'#1090#1101':'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 192.000000000000000000
      end
      object DBGrid2: TDBGrid
        Left = 4
        Top = 104
        Width = 417
        Height = 307
        Anchors = [akLeft, akTop, akBottom]
        DataSource = DataModuleMain.dsAvgFuelCons
        TabOrder = 1
        TitleFont.Charset = RUSSIAN_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Lucida Sans Unicode'
        TitleFont.Style = []
      end
      object avgFuelChart: TChart
        Left = 428
        Top = 4
        Width = 560
        Height = 407
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1059#1076#1077#1083#1100#1085#1099#1081' '#1088#1072#1089#1093#1086#1076' '#1090#1086#1087#1083#1080#1074#1072' '#1085#1072' '#1086#1090#1087#1091#1089#1082' '#1101#1083#1077#1082#1090#1088#1086#1101#1085#1077#1088#1075#1080#1080)
        Legend.Visible = False
        View3D = False
        Color = 12304821
        TabOrder = 2
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object qpar_edit: TsCalcEdit
        Left = 136
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 3
        BoundLabel.Active = True
        BoundLabel.Caption = 'Q '#1087#1072#1088':'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 192.000000000000000000
      end
      object DBNavigator1: TDBNavigator
        Left = 4
        Top = 76
        Width = 140
        Height = 25
        DataSource = DataModuleMain.dsAvgFuelCons
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 4
      end
      object BeeSeriesVisibleCheckbox: TsCheckBox
        Left = 320
        Top = 8
        Width = 50
        Height = 24
        Caption = 'bee'
        Checked = True
        State = cbChecked
        TabOrder = 5
        OnClick = BeeSeriesVisibleCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object EvSeriesVisibleCheckbox: TsCheckBox
        Left = 320
        Top = 28
        Width = 37
        Height = 24
        Caption = 'Ev'
        Checked = True
        State = cbChecked
        TabOrder = 6
        OnClick = EvSeriesVisibleCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object GgSeriesVisibleCheckbox: TsCheckBox
        Left = 320
        Top = 48
        Width = 42
        Height = 24
        Caption = 'Gg'
        Checked = True
        State = cbChecked
        TabOrder = 7
        OnClick = GgSeriesVisibleCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object QotSeriesVisibleCheckbox: TsCheckBox
        Left = 376
        Top = 8
        Width = 48
        Height = 24
        Caption = 'Qot'
        Checked = True
        State = cbChecked
        TabOrder = 8
        OnClick = QotSeriesVisibleCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object EsnSeriesVisibleCheckbox: TsCheckBox
        Left = 376
        Top = 28
        Width = 47
        Height = 24
        Caption = 'Esn'
        Checked = True
        State = cbChecked
        TabOrder = 9
        OnClick = EsnSeriesVisibleCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = 'Qot'
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      DesignSize = (
        993
        415)
      object DBGrid1: TDBGrid
        Left = 4
        Top = 32
        Width = 417
        Height = 379
        Anchors = [akLeft, akTop, akBottom]
        DataSource = DataModuleMain.dsQot
        TabOrder = 0
        TitleFont.Charset = RUSSIAN_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Lucida Sans Unicode'
        TitleFont.Style = []
      end
      object chtQot: TChart
        Left = 428
        Top = 32
        Width = 560
        Height = 379
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1054#1090#1087#1091#1089#1082' '#1090#1077#1087#1083#1072' '#1085#1072' '#1086#1090#1086#1087#1083#1077#1085#1080#1077' '#1075#1086#1088#1086#1076#1072' '#1089' '#1082#1086#1083#1083#1077#1082#1090#1086#1088#1086#1074' '#1089#1090#1072#1085#1094#1080#1080)
        Legend.Alignment = laBottom
        View3D = False
        Color = clAqua
        TabOrder = 1
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object QotVisibilityCheckbox: TsCheckBox
        Left = 428
        Top = 4
        Width = 48
        Height = 24
        Caption = 'Qot'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = QotVisibilityCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object QotSevVisibilityCheckBox: TsCheckBox
        Left = 488
        Top = 4
        Width = 99
        Height = 24
        Caption = 'Qot '#1089#1077#1074#1077#1088
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = QotSevVisibilityCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object QotUgVisibilityCheckbox: TsCheckBox
        Left = 596
        Top = 4
        Width = 74
        Height = 24
        Caption = 'Qot '#1102#1075
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = QotUgVisibilityCheckboxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object DBNavigator2: TDBNavigator
        Left = 4
        Top = 4
        Width = 140
        Height = 25
        DataSource = DataModuleMain.dsQot
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 5
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #1069#1089#1085
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      DesignSize = (
        993
        415)
      object K21Edit: TsCalcEdit
        Left = 4
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = #1050' '#1103#1095'21:'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 18.000000000000000000
      end
      object K38Edit: TsCalcEdit
        Left = 132
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #1050' '#1103#1095' 38:'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 18.000000000000000000
      end
      object dbgrd2: TDBGrid
        Left = 4
        Top = 112
        Width = 417
        Height = 299
        Anchors = [akLeft, akTop, akBottom]
        DataSource = DataModuleMain.dsEsn
        TabOrder = 7
        TitleFont.Charset = RUSSIAN_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Lucida Sans Unicode'
        TitleFont.Style = []
      end
      object chtEsn: TChart
        Left = 428
        Top = 76
        Width = 560
        Height = 335
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1056#1072#1089#1093#1086#1076' '#1101#1083#1077#1082#1090#1088#1086#1101#1085#1077#1088#1075#1080#1080' '#1085#1072' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1099#1077' '#1085#1091#1078#1076#1099)
        Legend.Alignment = laBottom
        View3D = False
        Color = 16755455
        TabOrder = 6
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object K52Edit: TsCalcEdit
        Left = 260
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = #1050' '#1103#1095'52:'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 18.000000000000000000
      end
      object K74Edit: TsCalcEdit
        Left = 388
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 3
        BoundLabel.Active = True
        BoundLabel.Caption = #1050' '#1103#1095' 74:'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 18.000000000000000000
      end
      object K2RAEdit: TsCalcEdit
        Left = 516
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 4
        BoundLabel.Active = True
        BoundLabel.Caption = #1050' '#1103#1095' 2'#1056#1040':'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 36.000000000000000000
      end
      object K4RBEdit: TsCalcEdit
        Left = 644
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 5
        BoundLabel.Active = True
        BoundLabel.Caption = #1050' '#1103#1095' 4'#1056#1042':'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 36.000000000000000000
      end
      object Esn21VisibleCheckBox: TsCheckBox
        Left = 780
        Top = 4
        Width = 40
        Height = 24
        Caption = '21'
        Checked = True
        State = cbChecked
        TabOrder = 8
        OnClick = Esn21VisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object Esn38VisibleCheckBox: TsCheckBox
        Left = 780
        Top = 24
        Width = 40
        Height = 24
        Caption = '38'
        Checked = True
        State = cbChecked
        TabOrder = 9
        OnClick = Esn38VisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object Esn52VisibleCheckBox: TsCheckBox
        Left = 780
        Top = 44
        Width = 40
        Height = 24
        Caption = '52'
        Checked = True
        State = cbChecked
        TabOrder = 10
        OnClick = Esn52VisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object Esn74VisibleCheckBox: TsCheckBox
        Left = 836
        Top = 4
        Width = 40
        Height = 24
        Caption = '74'
        Checked = True
        State = cbChecked
        TabOrder = 11
        OnClick = Esn74VisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object Esn2RaVisibleCheckBox: TsCheckBox
        Left = 836
        Top = 24
        Width = 49
        Height = 24
        Caption = '2Ra'
        Checked = True
        State = cbChecked
        TabOrder = 12
        OnClick = Esn2RaVisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object Esn4RbVisibleCheckBox: TsCheckBox
        Left = 836
        Top = 44
        Width = 50
        Height = 24
        Caption = '4Rb'
        Checked = True
        State = cbChecked
        TabOrder = 13
        OnClick = Esn4RbVisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object EsnVisibilityCheckBox: TsCheckBox
        Left = 896
        Top = 4
        Width = 48
        Height = 24
        Caption = #1069#1089#1085
        Checked = True
        State = cbChecked
        TabOrder = 14
        OnClick = EsnVisibilityCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object DBNavigator3: TDBNavigator
        Left = 4
        Top = 80
        Width = 140
        Height = 25
        DataSource = DataModuleMain.dsEsn
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 15
      end
    end
    object EvCalculationSheet: TsTabSheet
      Caption = #1069#1074
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      DesignSize = (
        993
        415)
      object chtEv: TChart
        Left = 428
        Top = 4
        Width = 560
        Height = 407
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1042#1099#1088#1072#1073#1086#1090#1082#1072' '#1101#1083#1077#1082#1090#1088#1086#1101#1085#1077#1088#1075#1080#1080)
        Legend.Visible = False
        View3D = False
        Color = 7864319
        TabOrder = 0
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object dbgrd1: TDBGrid
        Left = 4
        Top = 104
        Width = 417
        Height = 307
        Anchors = [akLeft, akTop, akBottom]
        DataSource = DataModuleMain.dsEv
        TabOrder = 3
        TitleFont.Charset = RUSSIAN_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Lucida Sans Unicode'
        TitleFont.Style = []
      end
      object Ctg1Edit: TsCalcEdit
        Left = 4
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #1050#1090#1075'1:'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 160.000000000000000000
      end
      object Ctg2Edit: TsCalcEdit
        Left = 132
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = #1050#1090#1075'2:'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 160.000000000000000000
      end
      object Ev2VisibleCheckBox: TsCheckBox
        Left = 372
        Top = 28
        Width = 49
        Height = 24
        Caption = 'EV2'
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = Ev2VisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object EVVisibleCheckBox: TsCheckBox
        Left = 372
        Top = 48
        Width = 39
        Height = 24
        Caption = 'EV'
        Checked = True
        State = cbChecked
        TabOrder = 5
        OnClick = EVVisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object Ev1VisibleCheckBox: TsCheckBox
        Left = 372
        Top = 8
        Width = 49
        Height = 24
        Caption = 'EV1'
        Checked = True
        State = cbChecked
        TabOrder = 6
        OnClick = Ev1VisibleCheckBoxClick
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object DBNavigator4: TDBNavigator
        Left = 4
        Top = 76
        Width = 140
        Height = 25
        DataSource = DataModuleMain.dsEv
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 7
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = 'G'#1075
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      DesignSize = (
        993
        415)
      object DBGrid3: TDBGrid
        Left = 4
        Top = 100
        Width = 417
        Height = 311
        Anchors = [akLeft, akTop, akBottom]
        DataSource = DataModuleMain.dsGas
        TabOrder = 0
        TitleFont.Charset = RUSSIAN_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Lucida Sans Unicode'
        TitleFont.Style = []
      end
      object chtGas: TChart
        Left = 428
        Top = 4
        Width = 560
        Height = 407
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1056#1072#1089#1093#1086#1076' '#1075#1072#1079#1072)
        Legend.Visible = False
        View3D = False
        Color = 12304821
        TabOrder = 1
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object qg_edit: TsCalcEdit
        Left = 4
        Top = 32
        Width = 121
        Height = 29
        AutoSize = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = 'q'#1075':'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = RUSSIAN_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -16
        BoundLabel.Font.Name = 'Lucida Sans Unicode'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        Value = 8100.000000000000000000
      end
      object DBNavigator5: TDBNavigator
        Left = 4
        Top = 72
        Width = 140
        Height = 25
        DataSource = DataModuleMain.dsGas
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 3
      end
    end
  end
  object StartPeriodDateedit: TsDateEdit
    Left = 4
    Top = 116
    Width = 145
    Height = 29
    AutoSize = False
    EditMask = '!99/99/9999;1; '
    MaxLength = 10
    TabOrder = 1
    Text = '  .  .    '
    BoundLabel.Active = True
    BoundLabel.Caption = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = RUSSIAN_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -16
    BoundLabel.Font.Name = 'Lucida Sans Unicode'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
  object EndPeriodDateEdit: TsDateEdit
    Left = 156
    Top = 116
    Width = 145
    Height = 29
    AutoSize = False
    EditMask = '!99/99/9999;1; '
    MaxLength = 10
    TabOrder = 2
    Text = '  .  .    '
    BoundLabel.Active = True
    BoundLabel.Caption = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = RUSSIAN_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -16
    BoundLabel.Font.Name = 'Lucida Sans Unicode'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
  object sGroupBox1: TsGroupBox
    Left = 700
    Top = 8
    Width = 301
    Height = 157
    Anchors = [akTop, akRight]
    Caption = #1051#1077#1075#1077#1085#1076#1072
    TabOrder = 4
    SkinData.SkinSection = 'GROUPBOX'
    object sLabel2: TsLabel
      Left = 12
      Top = 28
      Width = 255
      Height = 20
      Caption = 'b'#1101#1101' - '#1091#1076#1077#1083#1100#1085#1099#1081' '#1088#1072#1089#1093#1086#1076' '#1090#1086#1087#1083#1080#1074#1072
    end
    object sLabel3: TsLabel
      Left = 12
      Top = 52
      Width = 151
      Height = 20
      Caption = 'Qot - '#1086#1090#1087#1091#1089#1082' '#1090#1077#1087#1083#1072
    end
    object sLabel4: TsLabel
      Left = 12
      Top = 76
      Width = 233
      Height = 20
      Caption = #1069#1089#1085' - '#1088#1072#1089#1093#1086#1076' '#1085#1072' '#1089#1086#1073#1089'. '#1085#1091#1078#1076#1099
    end
    object sLabel5: TsLabel
      Left = 12
      Top = 100
      Width = 267
      Height = 20
      Caption = #1069#1074'   - '#1074#1099#1088#1072#1073#1086#1090#1082#1072' '#1101#1083#1077#1082#1090#1088#1086#1101#1085#1077#1088#1075#1080#1080
    end
    object sLabel6: TsLabel
      Left = 14
      Top = 124
      Width = 141
      Height = 20
      Caption = 'G'#1075'  - '#1088#1072#1089#1093#1086#1076' '#1075#1072#1079#1072
    end
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'GROUPBOX'
    Left = 665
    Top = 549
  end
end
