unit datamodule_main;

interface

uses
  SysUtils, Classes, ZConnection, DB, ZAbstractRODataset, ZAbstractDataset,
  ZAbstractTable, ZDataset,
  meter_value_list, kbmMemTable;

type
  TDataModuleMain = class(TDataModule)
    dsCoefficients: TDataSource;
    CoefficientsTable: TZTable;
    elsch_connection: TZConnection;
    CoefficientsTableid: TIntegerField;
    CoefficientsTablebte: TFloatField;
    CoefficientsTableqg: TFloatField;
    CoefficientsTableqm: TFloatField;
    CoefficientsTablecreated_at: TDateTimeField;
    CoefficientsTableupdated_at: TDateTimeField;
    MetersTable: TZTable;
    dsMeters: TDataSource;
    ChanelsTable: TZTable;
    dsChanels: TDataSource;
    ValuesTable: TZTable;
    dsValues: TDataSource;
    MetersTableid: TIntegerField;
    MetersTablecaption: TStringField;
    ChanelsTableid: TIntegerField;
    ChanelsTablemeter_id: TIntegerField;
    ChanelsTablecaption: TStringField;
    ChanelsTablemeter_chanel_no: TIntegerField;
    ValuesTableid: TLargeintField;
    ValuesTablechanel_id: TIntegerField;
    ValuesTablevalue: TFloatField;
    ValuesTabledate_query: TDateTimeField;
    GSSConnection: TZConnection;
    ParamsTable: TZTable;
    dsParams: TDataSource;
    ParamsTableid: TIntegerField;
    ParamsTabledate_query: TDateTimeField;
    ParamsTablevalue: TFloatField;
    kbmEvTable: TkbmMemTable;
    kbmEvTableNo: TIntegerField;
    kbmEvTableCurTime: TDateTimeField;
    kbmEvTableEv: TFloatField;
    kbmEvTableCtg1: TFloatField;
    kbmEvTableCtg1p: TFloatField;
    kbmEvTableCtg2: TFloatField;
    kbmEvTableCtg2p: TFloatField;
    dsEv: TDataSource;
    kbmEsn: TkbmMemTable;
    kbmEsnNo: TIntegerField;
    kbmEsnCurTime: TDateTimeField;
    kbmEsnEsn: TFloatField;
    dsEsn: TDataSource;
    kbmEsnCsn21: TFloatField;
    kbmEsnCsn21p: TFloatField;
    kbmEsnCsn38: TFloatField;
    kbmEsnCsn38p: TFloatField;
    kbmEsnCsn52: TFloatField;
    kbmEsnCsn52p: TFloatField;
    kbmEsnCsn74: TFloatField;
    kbmEsnCsn74p: TFloatField;
    kbmEsnCsn2Ra: TFloatField;
    kbmEsnCsn2Rap: TFloatField;
    kbmEsnCsn4Rb: TFloatField;
    kbmEsnCsn4Rbp: TFloatField;
    ChanelValsQuery: TZQuery;
    dsEvQuery: TDataSource;
    kbmEvTableEv1: TFloatField;
    kbmEvTableEv2: TFloatField;
    kbmEsnEsn21: TFloatField;
    kbmEsnEsn38: TFloatField;
    kbmEsnEsn52: TFloatField;
    kbmEsnEsn74: TFloatField;
    kbmEsnEsn2Ra: TFloatField;
    kbmEsnEsn4Rb: TFloatField;
    waterarc_conn: TZConnection;
    SeverUgArc: TZTable;
    dsSeverUgArc: TDataSource;
    SeverUgArcdate_arc: TDateTimeField;
    SeverUgArctotal_heat_used: TFloatField;
    SeverUgArcheat_for_pipe1: TFloatField;
    SeverUgArcheat_for_pipe2: TFloatField;
    SeverUgArcheat_for_pipe3: TFloatField;
    SeverUgArcvolume_for_pipe1: TFloatField;
    SeverUgArcvolume_for_pipe2: TFloatField;
    SeverUgArcvolume_for_pipe3: TFloatField;
    SeverUgArcavg_temperature_for_pipe1: TFloatField;
    SeverUgArcavg_temperature_for_pipe2: TFloatField;
    SeverUgArcavg_temperature_for_pipe3: TFloatField;
    SeverUgArcavg_pressure_for_pipe1: TFloatField;
    SeverUgArcavg_pressure_for_pipe2: TFloatField;
    SeverUgArcavg_pressure_for_pipe3: TFloatField;
    SeverUgArccold_water_temperature: TFloatField;
    SeverUgArcheat_for_pipe4: TFloatField;
    SeverUgArcheat_for_pipe5: TFloatField;
    SeverUgArcheat_for_pipe6: TFloatField;
    SeverUgArcvolume_for_pipe4: TFloatField;
    SeverUgArcvolume_for_pipe5: TFloatField;
    SeverUgArcvolume_for_pipe6: TFloatField;
    SeverUgArcavg_temperature_for_pipe4: TFloatField;
    SeverUgArcavg_temperature_for_pipe5: TFloatField;
    SeverUgArcavg_temperature_for_pipe6: TFloatField;
    SeverUgArcavg_pressure_for_pipe4: TFloatField;
    SeverUgArcavg_pressure_for_pipe5: TFloatField;
    SeverUgArcavg_pressure_for_pipe6: TFloatField;
    SeverUgArcpk_sua: TIntegerField;
    kbmQot: TkbmMemTable;
    dsQot: TDataSource;
    kbmQotNo: TIntegerField;
    kbmQotQot: TFloatField;
    kbmQotQsev: TFloatField;
    kbmQotQug: TFloatField;
    kbmQotGprSev: TFloatField;
    kbmQotGobSev: TFloatField;
    kbmQotGprUg: TFloatField;
    kbmQotGobUg: TFloatField;
    kbmQotThol: TFloatField;
    kbmQotTprSev: TFloatField;
    kbmQotTobSev: TFloatField;
    kbmQotTprUg: TFloatField;
    kbmQotTobUg: TFloatField;
    QotQuery: TZQuery;
    dsQotQuery: TDataSource;
    kbmQotDateArc: TDateTimeField;
    SeverUgArcarc_code: TIntegerField;
    kbmGas: TkbmMemTable;
    kbmGasNo: TIntegerField;
    kbmGasDateCalc: TDateTimeField;
    dsGas: TDataSource;
    kbmGasGg: TFloatField;
    GasQuery: TZQuery;
    GasQueryid: TIntegerField;
    GasQuerydate_query: TDateTimeField;
    GasQueryvalue: TFloatField;
    kbmAvgFuelCons: TkbmMemTable;
    kbmAvgFuelConsNo: TIntegerField;
    kbmAvgFuelConsDateCalc: TDateTimeField;
    kbmAvgFuelConsGg: TFloatField;
    dsAvgFuelCons: TDataSource;
    kbmAvgFuelConsbee: TFloatField;
    kbmAvgFuelConsqot: TFloatField;
    kbmAvgFuelConsev: TFloatField;
    kbmAvgFuelConsesn: TFloatField;
    procedure CoefficientsTableAfterInsert(DataSet: TDataSet);
    procedure CoefficientsTableBeforePost(DataSet: TDataSet);
    procedure ValuesTableFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FStartPeriod: TDateTime;
    FEndPeriod: TDateTime;
    FValuesFilterChanelId: integer;
    procedure FindChanelValsFor(ChanelId: integer);
    procedure CalculateNewCsnForChanel(ChanelId: integer;
      AFieldPart: string; Coef: Single);
    procedure CalculateCurrentCsnForChanel(ChanelId: integer;
      AFieldPart: string; Coef: Single);

  public
    { Public declarations }
    constructor Create(AOwner: TComponent);override;

    procedure CalculateAvgFuelConsumption( bte, qpar : single );
    procedure CalculateEv( CoefTg1, CoefTg2: Single );
    procedure CalculateEsn(K21, K38, K52, K74, K2RA, K4RB: Single);
    procedure CalculateGasConsumption( qg : single );
    procedure CalculateQot();
    procedure ClearSeverUgArcFilter();

    procedure OpenElectroschemeData;
    procedure OpenCoefficients;
    procedure OpenGSSData;
    procedure OpenWaterArc;

    procedure FillMeterValues(const values: TMeterValueList);
    procedure FilterSeverUgArc(arcCode: integer);

    function GetCurrentMeterCaption() : string;
    function GetCurrentChanelCaption() : string;

    procedure SetDatePeriod( startPeriod, endPeriod : TDateTime );
  end;

var
  DataModuleMain: TDataModuleMain;

implementation

uses
  data_meterValue, factory_Meter, const_archives, DateUtils;

const
  SQL_GET_CHANELS_VALUES = 'select id, chanel_id, value, date_query from public."values" ' +
                    'where (date_query >= ''%s'')and(date_query < ''%s'')and(chanel_id = %d)';

  SQL_GET_QOT_VALUES = 'select pk_sua, date_arc, heat_for_pipe1, heat_for_pipe2, heat_for_pipe5, heat_for_pipe6, ' +
                        'avg_temperature_for_pipe1, avg_temperature_for_pipe2, avg_temperature_for_pipe5, avg_temperature_for_pipe6, ' +
                        'cold_water_temperature, total_heat_used ' +
                        'from sever_ug_arc ' +
                        'where (date_arc >= ''%s'')and(date_arc < ''%s'')and(arc_code = %d)';


const
  MAX_ESN = 20;

{$R *.dfm}

{ TDataModuleMain }

procedure TDataModuleMain.OpenCoefficients;
begin
    CoefficientsTable.Open;
end;

procedure TDataModuleMain.CoefficientsTableAfterInsert(DataSet: TDataSet);
begin
    CoefficientsTablecreated_at.AsDateTime := now;
end;

procedure TDataModuleMain.CoefficientsTableBeforePost(DataSet: TDataSet);
begin
    CoefficientsTableupdated_at.AsDateTime := Now;
end;

procedure TDataModuleMain.OpenElectroschemeData();
begin
    FStartPeriod := Now - 7;
    FEndPeriod := Now;

    MetersTable.Open;
    ChanelsTable.Open;
    ValuesTable.Open;
end;


procedure TDataModuleMain.OpenGSSData;
begin
    ParamsTable.Open;
end;

procedure TDataModuleMain.ValuesTableFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin

  Accept := false;

  if (ValuesTabledate_query.AsDateTime > FStartPeriod )
    and (ValuesTabledate_query.AsDateTime <= FEndPeriod )
  then
  begin
    if(FValuesFilterChanelId > 0)then
    begin
        if(ValuesTablechanel_id.Value = FValuesFilterChanelId)then
            Accept := true;
    end else
        Accept := true;
  end;
end;

procedure TDataModuleMain.SetDatePeriod(startPeriod,
  endPeriod: TDateTime);
begin
    FStartPeriod := startPeriod;
    FEndPeriod := endPeriod + 1;
end;

procedure TDataModuleMain.FillMeterValues( const values: TMeterValueList );
    var
      newValue: TMeterValue;
begin
    ValuesTable.DisableControls;
    try
        while not ValuesTable.Eof do
        begin
          newValue := MeterFactory.MakeValue();
          newValue.ChanelId := ValuesTablechanel_id.AsInteger;
          newValue.Value := ValuesTablevalue.AsFloat;
          newValue.DateQuery := ValuesTabledate_query.AsDateTime;

          values.AddValue( newValue );
          ValuesTable.Next();
        end;
    finally
        ValuesTable.EnableControls;
    end;

    ValuesTable.First();
end;


function TDataModuleMain.GetCurrentChanelCaption: string;
begin
    Result := ChanelsTableid.AsString + ' : ' + ChanelsTablecaption.AsString;
end;

function TDataModuleMain.GetCurrentMeterCaption: string;
begin
    Result := MetersTablecaption.AsString;
end;

procedure TDataModuleMain.CalculateNewCsnForChanel( ChanelId: integer; AFieldPart: string; Coef: Single );
    var
      prevVal: Single;
      curVal: Single;
      Index: integer;
      Esn: Single;
begin
    kbmEsn.First();
    FindChanelValsFor( ChanelId );

    Index := 1;

    ChanelValsQuery.First();
    while not ChanelValsQuery.Eof do
    begin
        curVal := ChanelValsQuery.FieldByName('value').AsFloat;
        ChanelValsQuery.Next();
        prevVal := ChanelValsQuery.FieldByName('value').AsFloat;

        Esn := ( curVal - prevval )*Coef;
        if( abs(Esn) > MAX_ESN)then
            Esn := -1;

        kbmEsn.Append();

        kbmEsnNo.Value := Index;
        kbmEsnCurTime.Value := ChanelValsQuery.FieldByName('date_query').AsDateTime;

        kbmEsn.FieldByName('Csn' + AFieldPart).AsFloat := curVal;
        kbmEsn.FieldByName('Csn' + AFieldPart + 'p' ).AsFloat := prevVal;
        kbmEsn.FieldByName('Esn' + AFieldPart).AsFloat := Esn;

        kbmEsn.Post();

        Inc( Index );
    end;

    kbmEsn.First();
end;


procedure TDataModuleMain.CalculateEsn( K21, K38, K52, K74, K2RA, K4RB: Single);
    const
      K21Id = 57;
      K38Id = 61;
      K52Id = 65;
      K74Id = 69;
      K2RaId = 73;
      K4RbId = 77;
begin
    kbmEsn.DisableControls();
    kbmEsn.Active := false;
    kbmEsn.Active := true;

    CalculateNewCsnForChanel( K21Id, '21', K21 );
    CalculateCurrentCsnForChanel( K38Id, '38', K38 );
    CalculateCurrentCsnForChanel( K52Id, '52', K52 );
    CalculateCurrentCsnForChanel( K74Id, '74', K74 );
    CalculateCurrentCsnForChanel( K2RaId, '2RA', K2Ra );
    CalculateCurrentCsnForChanel( K4RbId, '4Rb', K4Rb );

    kbmEsn.First();
    while not kbmEsn.Eof do
    begin
        kbmEsn.Edit();
        kbmEsnEsn.Value :=
            kbmEsnEsn21.Value +
            kbmEsnEsn38.Value +
            kbmEsnEsn52.Value +
            kbmEsnEsn74.Value +
            kbmEsnEsn2Ra.Value +
            kbmEsnEsn4Rb.Value;

        kbmEsn.Post();

        kbmEsn.Next();
    end;

    kbmEsn.First();

    kbmEsn.EnableControls();
end;

procedure TDataModuleMain.FindChanelValsFor( ChanelId: integer );
begin
    ChanelValsQuery.SQL.Clear();
    ChanelValsQuery.SQL.Add(
        Format(
            SQL_GET_CHANELS_VALUES,
            [
                FormatDateTime('yyyymmdd',FStartPeriod),
                FormatDateTime('yyyymmdd',FEndPeriod),
                ChanelId
            ]
        )
    );
    ChanelValsQuery.Open();
end;

procedure TDataModuleMain.CalculateEv( CoefTg1, CoefTg2: Single );
    const
        MAX_EV = 200;
    const
        GENERATOR1_AMINUS = 54;
        GENERATOR2_AMINUS = 52;

    var
        Stg1, Stg1p: Double;
        Stg2, Stg2p: Double;
        I: integer;
        Ev: Single;
begin
    kbmEvTable.DisableControls();
    ChanelValsQuery.DisableControls();

    FindChanelValsFor( GENERATOR1_AMINUS );

    kbmEvTable.Close();
    kbmEvTable.Open();

    ChanelValsQuery.First();
    while not ChanelValsQuery.Eof do
    begin
        Stg1 := ChanelValsQuery.FieldByName('value').Value;

        ChanelValsQuery.Next();

        Stg1p := ChanelValsQuery.FieldByName('value').Value;

        Ev := (Stg1 - Stg1p)*CoefTg1;
        if(Abs(Ev) > MAX_EV)then
            Ev := -1;

        kbmEvTable.Append();
        kbmEvTableCurTime.Value := ChanelValsQuery.FieldByName('date_query').Value;
        kbmEvTableCtg1.Value := Stg1;
        kbmEvTableCtg1p.Value := Stg1p;

        kbmEvTableEv1.Value :=  Ev;

        kbmEvTable.Post();
    end;

    FindChanelValsFor( GENERATOR2_AMINUS );

    kbmEvTable.First();

    I := 1;

    ChanelValsQuery.First();
    while not ChanelValsQuery.Eof do
    begin
        Stg2 := ChanelValsQuery.FieldByName('value').Value;

        ChanelValsQuery.Next();

        Stg2p := ChanelValsQuery.FieldByName('value').Value;

        Ev := (Stg2 - Stg2p)*CoefTg2;
        if(Abs(Ev) > MAX_EV)then
            Ev := -1;

        kbmEvTable.Edit();
        kbmEvTableNo.Value := I;
        kbmEvTableCtg2.Value := Stg2;
        kbmEvTableCtg2p.Value := Stg2p;

        kbmEvTableEv2.Value := Ev;

        kbmEvTableEv.Value :=  kbmEvTableEv1.Value + kbmEvTableEv2.Value;

        kbmEvTable.Post();
        kbmEvTable.Next();

        Inc(I);
    end;

    kbmEvTable.First();
    ChanelValsQuery.First();

    kbmEvTable.EnableControls();
    ChanelValsQuery.EnableControls();
end;

constructor TDataModuleMain.Create(AOwner: TComponent);
begin
  inherited;

  FValuesFilterChanelId := 0;
end;

procedure TDataModuleMain.CalculateCurrentCsnForChanel(ChanelId: integer;
  AFieldPart: string; Coef: Single );
    var
      prevVal: Single;
      curVal: Single;
      Esn: Single;
begin
    kbmEsn.First();
    FindChanelValsFor( ChanelId );


    kbmEsn.First();
    ChanelValsQuery.First();
    while not ChanelValsQuery.Eof do
    begin
        curVal := ChanelValsQuery.FieldByName('value').AsFloat;
        ChanelValsQuery.Next();
        prevVal := ChanelValsQuery.FieldByName('value').AsFloat;

        Esn := (curVal - prevVal)*Coef;
        if( Abs(Esn) > MAX_ESN )then
            Esn := -1;

        kbmEsn.Edit();

        kbmEsn.FieldByName('Csn' + AFieldPart).AsFloat := curVal;
        kbmEsn.FieldByName('Csn' + AFieldPart + 'p' ).AsFloat := prevVal;
        kbmEsn.FieldByName('Esn' + AFieldPart).AsFloat := Esn;

        kbmEsn.Post();

        kbmEsn.Next();
    end;

    kbmEsn.First();
end;


procedure TDataModuleMain.OpenWaterArc;
begin
    SeverUgArc.Open();
end;

procedure TDataModuleMain.CalculateQot();
    type
        THeatValues = record
            Gpr : double;
            Gob : double;
            Tpr : double;
            Tob : double;
        end;

    var
        i : integer;
        Thol : double;
        SevVals : THeatValues;
        UgVals : THeatValues;

begin

  kbmQot.DisableControls();
  kbmQot.Active := false;
  kbmQot.Active := true;

  QotQuery.Active := false;
  QotQuery.SQL.Text :=
        Format(
            SQL_GET_QOT_VALUES,
            [
                FormatDateTime('yyyymmdd',FStartPeriod),
                FormatDateTime('yyyymmdd',FEndPeriod),
                SEVER_ARC
            ]);
  QotQuery.Open();

  i := 1;

  while not QotQuery.Eof do
  begin
    kbmQot.Append();
    kbmQotNo.Value := i;

    Thol := QotQuery.FieldByName('cold_water_temperature').AsFloat;

    SevVals.Gpr := QotQuery.FieldByName('heat_for_pipe1').AsFloat;
    SevVals.Gob := QotQuery.FieldByName('heat_for_pipe2').AsFloat;
    SevVals.Tpr := QotQuery.FieldByName('avg_temperature_for_pipe1').AsFloat;
    SevVals.Tob := QotQuery.FieldByName('avg_temperature_for_pipe2').AsFloat;


    kbmQotThol.Value := Thol;
    kbmQotDateArc.Value := QotQuery.FieldByName('date_arc').AsDateTime;

    kbmQotGprSev.Value := SevVals.Gpr;
    kbmQotGobSev.Value := SevVals.Gob;
    kbmQotTprSev.Value := SevVals.Tpr;
    kbmQotTobSev.Value := SevVals.Tob;
    kbmQotQsev.Value := QotQuery.FieldByName('total_heat_used').AsFloat;



    kbmQot.Post();


    QotQuery.Next();
    inc(i);
  end;


  QotQuery.Active := false;
  QotQuery.SQL.Text :=
        Format(
            SQL_GET_QOT_VALUES,
            [
                FormatDateTime('yyyymmdd',FStartPeriod),
                FormatDateTime('yyyymmdd',FEndPeriod),
                UG_ARC
            ]);
  QotQuery.Open();

  kbmQot.First();

  while not QotQuery.Eof do
  begin
    kbmQot.Edit();

    Thol := QotQuery.FieldByName('cold_water_temperature').AsFloat;

    UgVals.Gpr := QotQuery.FieldByName('heat_for_pipe1').AsFloat;
    UgVals.Gob := QotQuery.FieldByName('heat_for_pipe2').AsFloat;
    UgVals.Tpr := QotQuery.FieldByName('avg_temperature_for_pipe1').AsFloat;
    UgVals.Tob := QotQuery.FieldByName('avg_temperature_for_pipe2').AsFloat;

    kbmQotGprUg.Value := UgVals.Gpr;
    kbmQotGobUg.Value := UgVals.Gob;
    kbmQotTprUg.Value := UgVals.Tpr;
    kbmQotTobUg.Value := UgVals.Tob;
    kbmQotQug.Value := QotQuery.FieldByName('total_heat_used').AsFloat;

    kbmQotQot.Value := kbmQotQsev.Value + kbmQotQug.Value;

    kbmQot.Post();


    QotQuery.Next();
    kbmQot.Next();
  end;


  kbmQot.First();
  kbmQot.EnableControls();
end;

procedure TDataModuleMain.FilterSeverUgArc( arcCode : integer );
begin
    SeverUgArc.DisableControls();
    SeverUgArc.Filtered := false;
    SeverUgArc.Filter := 'arc_code = ' + IntToStr(arcCode);
    SeverUgArc.Filtered := true;
    SeverUgArc.EnableControls();
    //SeverUgArc.Refresh();
end;



procedure TDataModuleMain.ClearSeverUgArcFilter;
begin
    SeverUgArc.Filtered := false;
end;

procedure TDataModuleMain.CalculateAvgFuelConsumption( bte, qpar : single );
    const
      MINUTE_DIFF = 2;
    var
      curDate: TDateTime;
      Gg : single;
      Qot : single;
      Ev : single;
      Esn : single;
      bee : single;
      i : integer;

      function FindGgFor( date : TDateTime ) : single;
      begin
        Result := 0.0;

        kbmGas.First();
        if( kbmGas.Locate('datecalc', date, []))then
            Result := kbmGasGg.Value;
      end;

      function FindNearDateFor(
                    date : TDateTime;
                    minDiff : integer;
                    ds : TDataSet;
                    dateFieldName, valueFieldName : string
                    ) : Single;
        var
          oldDate : TDateTime;
          oldValue : single;
      begin
        Result := 0.0;

        ds.First();
        oldDate := ds.FieldByName(dateFieldName).AsDateTime;
        oldValue := ds.FieldByName(valueFieldName).AsFloat;

        while not ds.Eof do
        begin
            ds.Next();

            if( MinutesBetween( date, oldDate ) < minDiff )then
            begin
                Result := oldValue;
                break;
            end;

            oldDate := ds.FieldByName(dateFieldname).AsDateTime;
            oldValue := ds.FieldByName(valueFieldName).AsFloat;
        end;
      end;

      function FindQotFor( date : TDateTime ) : single;
      begin
        Result := FindNearDateFor( date, MINUTE_DIFF, kbmQot, 'DateArc', 'Qot' );
      end;

      function FindEvFor( date : TDateTime ) : single;
      begin
        Result := FindNearDateFor( date, MINUTE_DIFF, kbmEvTable, 'CurTime', 'Ev');
      end;

      function FindEsnFor( date : TDateTime ) : single;
      begin
        Result := FindNearDateFor( date, MINUTE_DIFF, kbmEsn, 'CurTime', 'Esn');
      end;
begin
    i := 1;

    kbmGas.DisableControls();
    kbmQot.DisableControls();
    kbmEsn.DisableControls();
    kbmEvTable.DisableControls();

    kbmAvgFuelCons.Close();
    kbmAvgFuelCons.Open();

    curDate := FStartPeriod;
    while curDate < FEndPeriod do
    begin
        Gg := FindGgFor( curDate );
        Qot := FindQotFor( curDate );
        Ev := FindEvFor( curDate );
        Esn := FindEsnFor( curDate );

        bee := 0.0;

        if( Ev - Esn <> 0 )then
            bee := (Gg/7000 - (Qot+Qpar)*bte/1000)*1000/(Ev - Esn);

        kbmAvgFuelCons.Append();
        kbmAvgFuelConsNo.Value := i;
        kbmAvgFuelConsDateCalc.Value := curDate;
        kbmAvgFuelConsbee.Value := bee;
        kbmAvgFuelConsqot.Value := qot;
        kbmAvgFuelConsev.Value := ev;
        kbmAvgFuelConsesn.Value := esn;
        kbmAvgFuelConsGg.Value := Gg;
        kbmAvgFuelCons.Post();

        curDate := IncHour( curDate, 1 );
        i := i + 1;
    end;

    kbmGas.EnableControls();
    kbmQot.EnableControls();
    kbmEsn.EnableControls();
    kbmEvTable.EnableControls();
end;

procedure TDataModuleMain.CalculateGasConsumption(qg: single);
  const
     FIRST_DATE = 'first_date';
     LAST_DATE = 'last_date';
  var
    i : integer;
begin
    kbmGas.Close();
    kbmGas.Open();

    GasQuery.Close();
    GasQuery.ParamByName(FIRST_DATE).Value := FStartPeriod;
    GasQuery.ParamByName(LAST_DATE).Value := FEndPeriod;
    GasQuery.Open();

    i := 0;

    while not GasQuery.Eof do
    begin
        i := i + 1;

        kbmGas.Append();
        kbmGasNo.Value := i;
        kbmGasDateCalc.Value := GasQuerydate_query.Value;
        kbmGasGg.Value := qg * GasQueryvalue.Value;
        kbmGas.Post();
        
      GasQuery.Next();
    end;
end;

end.
