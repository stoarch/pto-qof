unit data_meterValue;

interface

type
  TMeterValue = class
  private
    FChanelId: integer;
    FValue: real;
    FDateQuery: TDateTime;
    procedure SetChanelId(const Value: integer);
    procedure SetDateQuery(const Value: TDateTime);
    procedure SetValue(const Value: real);
  public

    property ChanelId : integer read FChanelId write SetChanelId;
    property Value : real read FValue write SetValue;
    property DateQuery : TDateTime read FDateQuery write SetDateQuery;
  end;

implementation

{ TMeterValue }

procedure TMeterValue.SetChanelId(const Value: integer);
begin
  FChanelId := Value;
end;

procedure TMeterValue.SetDateQuery(const Value: TDateTime);
begin
  FDateQuery := Value;
end;

procedure TMeterValue.SetValue(const Value: real);
begin
  FValue := Value;
end;

end.
