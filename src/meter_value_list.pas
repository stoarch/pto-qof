unit meter_value_list;

interface
uses
  data_meterValue,
  storage_objectList
  ;


type
  TMeterValueList = class( TObjectList )
    private
      function GetValue( index: integer ) : TMeterValue;
      procedure SetValue( index: integer; const value: TMeterValue );
    public
      procedure AddValue( value: TMeterValue );
       property Values[ index: integer ] : TMeterValue read GetValue write SetValue;
  end;

implementation

{ TMeterValueList }

procedure TMeterValueList.AddValue(value: TMeterValue);
begin
    AddObject( value );
end;

function TMeterValueList.GetValue(index: integer): TMeterValue;
begin
   Result := objects[ index ] as TMeterValue;
end;

procedure TMeterValueList.SetValue(index: integer;
  const value: TMeterValue);
begin
    objects[ index ] := value;
end;

end.
