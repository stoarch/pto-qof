unit frame_coeff_editor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, DBCtrls, Grids, DBGrids, StdCtrls, sLabel,
  sFrameAdapter, datamodule_main, Mask;

type
  TCoefficientsFrame = class(TFrame)
    sFrameAdapter1: TsFrameAdapter;
    sLabel1: TsLabel;
    dbgrd1: TDBGrid;
    dbnvgr1: TDBNavigator;
    lbl1: TLabel;
    dbedtbte: TDBEdit;
    lbl2: TLabel;
    dbedtqg: TDBEdit;
    lbl3: TLabel;
    dbedtqm: TDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute;
  end;

implementation

{$R *.dfm}

{ TCoefficientsFrame }

procedure TCoefficientsFrame.Execute;
begin
    DataModuleMain.OpenCoefficients;
end;

end.
