unit frame_massflowrate_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Grids, DBGrids, StdCtrls, sLabel, ExtCtrls, DBCtrls,
  sFrameAdapter, datamodule_main, sComboBox;

type
  TWaterFlowrateFrame = class(TFrame)
    sFrameAdapter1: TsFrameAdapter;
    dbnvgr1: TDBNavigator;
    sLabel1: TsLabel;
    dbgrd1: TDBGrid;
    selectChanelComboBox: TsComboBox;
    procedure selectChanelComboBoxChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute();
  end;

implementation

uses
  const_archives;

{$R *.dfm}

{ TWaterFlowrateFrame }

procedure TWaterFlowrateFrame.Execute;
begin
    DataModuleMain.OpenWaterArc();
end;

procedure TWaterFlowrateFrame.selectChanelComboBoxChange(Sender: TObject);
    const
        ALL_ITEMS = 0;
        SEVER_ITEMS = 1;
        UG_ITEMS = 2;

        C_CODES : array[1..2] of integer = (SEVER_ARC, UG_ARC);
begin
  if( selectChanelComboBox.ItemIndex = ALL_ITEMS )then
    DataModuleMain.ClearSeverUgArcFilter()
  else
    DataModuleMain.FilterSeverUgArc( C_CODES[ selectChanelComboBox.ItemIndex ]);
end;

end.
