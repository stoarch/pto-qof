object CoefficientsFrame: TCoefficientsFrame
  Left = 0
  Top = 0
  Width = 666
  Height = 211
  TabOrder = 0
  DesignSize = (
    666
    211)
  object sLabel1: TsLabel
    Left = 4
    Top = 80
    Width = 183
    Height = 16
    Caption = #1057#1087#1080#1089#1086#1082' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1086#1074':'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
  end
  object lbl1: TLabel
    Left = 4
    Top = 34
    Width = 15
    Height = 13
    Caption = 'bte'
    FocusControl = dbedtbte
  end
  object lbl2: TLabel
    Left = 116
    Top = 34
    Width = 12
    Height = 13
    Caption = 'qg'
    FocusControl = dbedtqg
  end
  object lbl3: TLabel
    Left = 228
    Top = 34
    Width = 14
    Height = 13
    Caption = 'qm'
    FocusControl = dbedtqm
  end
  object dbgrd1: TDBGrid
    Left = 4
    Top = 108
    Width = 656
    Height = 96
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataModuleMain.dsCoefficients
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object dbnvgr1: TDBNavigator
    Left = 3
    Top = 4
    Width = 240
    Height = 25
    DataSource = DataModuleMain.dsCoefficients
    TabOrder = 1
  end
  object dbedtbte: TDBEdit
    Left = 4
    Top = 50
    Width = 104
    Height = 21
    DataField = 'bte'
    DataSource = DataModuleMain.dsCoefficients
    TabOrder = 2
  end
  object dbedtqg: TDBEdit
    Left = 116
    Top = 50
    Width = 104
    Height = 21
    DataField = 'qg'
    DataSource = DataModuleMain.dsCoefficients
    TabOrder = 3
  end
  object dbedtqm: TDBEdit
    Left = 228
    Top = 50
    Width = 104
    Height = 21
    DataField = 'qm'
    DataSource = DataModuleMain.dsCoefficients
    TabOrder = 4
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'GROUPBOX'
    Left = 396
    Top = 280
  end
end
