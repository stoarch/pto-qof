unit form_meterGraphViewer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, TeeProcs, TeEngine,
  Chart, StdCtrls, sLabel, sSkinProvider, Series,
  meter_value_list, DB, DBGrids, kbmMemTable;

type
  TMeterGraphViewerForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    CaptionLabel: TsLabel;
    dateRangeLabel: TsLabel;
    chtGraph: TChart;
    Series1: TLineSeries;
    kbmGraphData: TkbmMemTable;
    dsGraphData: TDataSource;
    dbgrd1: TDBGrid;
    kbmGraphDataid: TIntegerField;
    kbmGraphDataDateQuery: TDateTimeField;
    kbmGraphDataValue: TFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetCaption( value: string );
    procedure SetData( values : TMeterValueList );
    procedure SetDateRange( startDate, endDate: TDateTime );
  end;

var
  MeterGraphViewerForm: TMeterGraphViewerForm;

implementation

{$R *.dfm}

{ TMeterGraphViewerForm }

procedure TMeterGraphViewerForm.SetCaption(value: string);
begin
    CaptionLabel.Caption := '�������:' + value;
end;

procedure TMeterGraphViewerForm.SetData(values: TMeterValueList);
    var
      series: TLineSeries;
      j: integer;
begin
    kbmGraphData.Close();
    kbmGraphData.Open();

    chtGraph.SeriesList.Clear;

    series := TLineSeries.create(self);
    series.Title := 'Graph';
    series.XValues.DateTime := true;

    series.LinePen.Width := 1;
    series.LinePen.Style :=  psSolid;
    series.LinePen.Color := clBlue;

    series.Pointer.Style := psCircle;
    series.Pointer.HorizSize := 3;
    series.Pointer.VertSize := 3;

    for j := 1 to values.count do
    begin
        series.AddXY( values.Values[j].DateQuery, values.Values[j].Value, '', clNavy );

        kbmGraphData.Append();
        kbmGraphDataid.AsInteger := j;
        kbmGraphDataDateQuery.AsDateTime := values.values[j].DateQuery;
        kbmGraphDataValue.AsFloat := values.Values[j].Value;
        kbmGraphData.Post();
    end;

    chtGraph.AddSeries( series );

end;

procedure TMeterGraphViewerForm.SetDateRange(startDate,
  endDate: TDateTime);
begin
    dateRangeLabel.Caption := '�������� �� ������ � ' +
        FormatDateTime( 'dd-mm-yyyy', startDate ) + ' �� ' +
        FormatDateTime( 'dd-mm-yyyy', endDate );
end;

end.
