unit form_main;

interface

uses
  // common //
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, sSkinProvider, sSkinManager, ActnList, StdCtrls, sLabel,
  ComCtrls, sPageControl, ImgList, Buttons, sBitBtn,
  // pto //
  frame_coeff_editor,
  frame_elsch_view,
  frame_gss_view,
  frame_massflowrate_view,
  frame_qotCalculator
  ;

type
    TFrameCreatorFunc = function () : TFrame of object;

  TPtoMainForm = class(TForm)
    MainMenu1: TMainMenu;
    sSkinManager1: TsSkinManager;
    MainSkinProvider: TsSkinProvider;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    FunctionPages: TsPageControl;
    sTabSheet1: TsTabSheet;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    actlstMain: TActionList;
    actEditCoefficients: TAction;
    actExit: TAction;
    ilFunctions: TImageList;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    actShowElectiricity: TAction;
    actDisplayGSSVals: TAction;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    actQotCalculation: TAction;
    sBitBtn5: TsBitBtn;
    actShowMassFlowrate: TAction;
    procedure editCoeffWebLabelClick(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actEditCoefficientsExecute(Sender: TObject);
    procedure actShowElectiricityExecute(Sender: TObject);
    procedure actDisplayGSSValsExecute(Sender: TObject);
    procedure actQotCalculationExecute(Sender: TObject);
    procedure actShowMassFlowrateExecute(Sender: TObject);
  private
    { Private declarations }
    FCoeffEditor : TCoefficientsFrame;
    FElshViewer : TElschViewFrame;
    FGSSViewer : TGSSViewerFrame;
    FQotCalculator: TQotCalculatorFrame;
    FWaterArcViewer: TWaterFlowrateFrame;

    procedure AddFunctionFrame( const frame : TFrame; acaption : string; frameCreator : TFrameCreatorFunc);
    function CreateCoeffEditor: TFrame;
    function CreateElshViewer: TFrame;
    function CreateGSSViewer: TFrame;
    function CreateQotCalculator: TFrame;
    function CreateWaterArcViewer: TFrame;
  public
    { Public declarations }
  end;

var
  PtoMainForm: TPtoMainForm;

implementation


{$R *.dfm}

procedure TPtoMainForm.editCoeffWebLabelClick(Sender: TObject);
begin
  actEditCoefficients.Execute;
end;

procedure TPtoMainForm.actExitExecute(Sender: TObject);
begin
    Close();
end;

procedure TPtoMainForm.AddFunctionFrame(
        const frame : TFrame;
        acaption : string;
        frameCreator : TFrameCreatorFunc );
    var
      newPage : TsTabSheet;
      newFrame : TFrame;
begin
    if( assigned( frame ) )then
    begin
      FunctionPages.ActivePage := TsTabSheet( frame.Parent );
      exit;
    end;

    newFrame := frameCreator();

    newPage := TsTabSheet.Create( FunctionPages );
    newPage.Caption := acaption;
    newPage.PageControl := FunctionPages;

    newFrame.Parent := newPage;
    newFrame.Align := alClient;

    FunctionPages.ActivePage := newPage;
end;

function TPtoMainForm.CreateCoeffEditor(): TFrame;
begin
    FCoeffEditor := TCoefficientsFrame.Create(self);
    Result := FCoeffEditor;
end;


procedure TPtoMainForm.actEditCoefficientsExecute(Sender: TObject);
begin

    AddFunctionFrame(
        FCoeffEditor,
        '�������� �������������',
        CreateCoeffEditor );

    FCoeffEditor.Execute;
end;

function TPtoMainForm.CreateElshViewer(): TFrame;
begin

    FElshViewer := TElschViewFrame.Create( self );

    Result := FElshViewer;
end;


procedure TPtoMainForm.actShowElectiricityExecute(Sender: TObject);
begin

    AddFunctionFrame(
        FElshViewer,
        '�������� ���������� ������������',
        CreateElshViewer );

    FElshViewer.Execute;
end;

function TPtoMainForm.CreateGSSViewer(): TFrame;
begin

    FGSSViewer := TGSSViewerFrame.Create( self );

    Result := FGSSViewer;
end;


procedure TPtoMainForm.actDisplayGSSValsExecute(Sender: TObject);
begin

   AddFunctionFrame(
        FGSSViewer,
        '�������� ���������� ���',
        CreateGSSViewer );

    FGSSViewer.Execute;
end;

procedure TPtoMainForm.actQotCalculationExecute(Sender: TObject);
begin
   AddFunctionFrame(
        FQotCalculator,
        '������ ��������� �����������',
        CreateQotCalculator );
end;

function TPtoMainForm.CreateQotCalculator(): TFrame;
begin
    FQotCalculator := TQotCalculatorFrame.Create( self );

    Result := FQotCalculator;
end;

function TPtoMainForm.CreateWaterArcViewer(): TFrame;
begin
   FWaterArcViewer := TWaterFlowrateFrame.Create(self);

   Result := FWaterArcViewer;
end;

procedure TPtoMainForm.actShowMassFlowrateExecute(Sender: TObject);
begin
  AddFunctionFrame(
    FWaterArcViewer,
    '����� ��������� �����-��',
    CreateWaterArcViewer
  );

  FWaterArcViewer.Execute();
end;

end.
