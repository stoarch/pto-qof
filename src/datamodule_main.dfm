object DataModuleMain: TDataModuleMain
  OldCreateOrder = False
  Left = 528
  Top = 174
  Height = 673
  Width = 712
  object dsCoefficients: TDataSource
    DataSet = CoefficientsTable
    Left = 124
    Top = 88
  end
  object CoefficientsTable: TZTable
    Connection = elsch_connection
    SortedFields = 'created_at'
    SortType = stDescending
    AfterInsert = CoefficientsTableAfterInsert
    BeforePost = CoefficientsTableBeforePost
    TableName = 'public.coefficients'
    IndexFieldNames = 'created_at Desc'
    Left = 28
    Top = 88
    object CoefficientsTableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object CoefficientsTablebte: TFloatField
      FieldName = 'bte'
    end
    object CoefficientsTableqg: TFloatField
      FieldName = 'qg'
    end
    object CoefficientsTableqm: TFloatField
      FieldName = 'qm'
    end
    object CoefficientsTablecreated_at: TDateTimeField
      FieldName = 'created_at'
      Required = True
    end
    object CoefficientsTableupdated_at: TDateTimeField
      FieldName = 'updated_at'
      Required = True
    end
  end
  object elsch_connection: TZConnection
    Protocol = 'postgresql-8'
    HostName = '192.168.254.125'
    Database = 'elsch_hef'
    User = 'scaner'
    Password = 'WinMainQ45'
    Properties.Strings = (
      'codepage=WIN1251'
      'client_encoding=WIN1251')
    Left = 24
    Top = 16
  end
  object MetersTable: TZTable
    Connection = elsch_connection
    TableName = 'public.meter_dict'
    Left = 224
    Top = 20
    object MetersTableid: TIntegerField
      FieldName = 'id'
      ReadOnly = True
      Required = True
    end
    object MetersTablecaption: TStringField
      FieldName = 'caption'
      Size = 50
    end
  end
  object dsMeters: TDataSource
    DataSet = MetersTable
    Left = 284
    Top = 20
  end
  object ChanelsTable: TZTable
    Connection = elsch_connection
    SortType = stIgnored
    TableName = 'public.chanel_dict'
    MasterFields = 'id'
    MasterSource = dsMeters
    LinkedFields = 'meter_id'
    Left = 224
    Top = 72
    object ChanelsTableid: TIntegerField
      FieldName = 'id'
      ReadOnly = True
      Required = True
    end
    object ChanelsTablemeter_id: TIntegerField
      FieldName = 'meter_id'
      ReadOnly = True
      Visible = False
    end
    object ChanelsTablecaption: TStringField
      FieldName = 'caption'
      Size = 50
    end
    object ChanelsTablemeter_chanel_no: TIntegerField
      FieldName = 'meter_chanel_no'
    end
  end
  object dsChanels: TDataSource
    DataSet = ChanelsTable
    Left = 284
    Top = 72
  end
  object ValuesTable: TZTable
    Connection = elsch_connection
    SortedFields = 'date_query'
    SortType = stDescending
    OnFilterRecord = ValuesTableFilterRecord
    Filtered = True
    ReadOnly = True
    TableName = 'public."values"'
    MasterFields = 'id'
    MasterSource = dsChanels
    LinkedFields = 'chanel_id'
    IndexFieldNames = 'date_query Desc'
    Left = 224
    Top = 140
    object ValuesTableid: TLargeintField
      FieldName = 'id'
      Required = True
    end
    object ValuesTablechanel_id: TIntegerField
      FieldName = 'chanel_id'
      Visible = False
    end
    object ValuesTablevalue: TFloatField
      FieldName = 'value'
      DisplayFormat = '#####0.####'
      Precision = 4
    end
    object ValuesTabledate_query: TDateTimeField
      FieldName = 'date_query'
    end
  end
  object dsValues: TDataSource
    DataSet = ValuesTable
    Left = 284
    Top = 140
  end
  object GSSConnection: TZConnection
    Protocol = 'postgresql-8'
    HostName = '192.168.254.125'
    Database = 'grs_hef'
    User = 'scaner'
    Password = 'WinMainQ45'
    Properties.Strings = (
      'codepage=WIN1251'
      'client_encoding=WIN1251')
    Connected = True
    Left = 408
    Top = 296
  end
  object ParamsTable: TZTable
    Connection = GSSConnection
    SortedFields = 'date_query'
    SortType = stDescending
    ReadOnly = True
    TableName = 'public.params'
    IndexFieldNames = 'date_query Desc'
    Left = 496
    Top = 296
    object ParamsTableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object ParamsTabledate_query: TDateTimeField
      FieldName = 'date_query'
    end
    object ParamsTablevalue: TFloatField
      FieldName = 'value'
    end
  end
  object dsParams: TDataSource
    DataSet = ParamsTable
    Left = 556
    Top = 296
  end
  object kbmEvTable: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'No'
        DataType = ftInteger
      end
      item
        Name = 'CurTime'
        DataType = ftDateTime
      end
      item
        Name = 'Ev'
        DataType = ftFloat
      end
      item
        Name = 'Ctg1'
        DataType = ftFloat
      end
      item
        Name = 'Ctg1p'
        DataType = ftFloat
      end
      item
        Name = 'Ctg2'
        DataType = ftFloat
      end
      item
        Name = 'Ctg2p'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 41
    Top = 301
    object kbmEvTableNo: TIntegerField
      DisplayWidth = 5
      FieldName = 'No'
    end
    object kbmEvTableCurTime: TDateTimeField
      DisplayWidth = 14
      FieldName = 'CurTime'
    end
    object kbmEvTableEv: TFloatField
      DisplayWidth = 8
      FieldName = 'Ev'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEvTableCtg1: TFloatField
      DisplayWidth = 13
      FieldName = 'Ctg1'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEvTableCtg1p: TFloatField
      DisplayWidth = 13
      FieldName = 'Ctg1p'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEvTableCtg2: TFloatField
      DisplayWidth = 13
      FieldName = 'Ctg2'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEvTableCtg2p: TFloatField
      DisplayWidth = 13
      FieldName = 'Ctg2p'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEvTableEv1: TFloatField
      FieldName = 'Ev1'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEvTableEv2: TFloatField
      FieldName = 'Ev2'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
  end
  object dsEv: TDataSource
    DataSet = kbmEvTable
    Left = 121
    Top = 301
  end
  object kbmEsn: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'No'
        DataType = ftInteger
      end
      item
        Name = 'CurTime'
        DataType = ftDateTime
      end
      item
        Name = 'Esn'
        DataType = ftFloat
      end
      item
        Name = 'Csn21'
        DataType = ftFloat
      end
      item
        Name = 'Csn21p'
        DataType = ftFloat
      end
      item
        Name = 'Csn38'
        DataType = ftFloat
      end
      item
        Name = 'Csn38p'
        DataType = ftFloat
      end
      item
        Name = 'Csn52'
        DataType = ftFloat
      end
      item
        Name = 'Csn52p'
        DataType = ftFloat
      end
      item
        Name = 'Csn74'
        DataType = ftFloat
      end
      item
        Name = 'Csn74p'
        DataType = ftFloat
      end
      item
        Name = 'Csn2RA'
        DataType = ftFloat
      end
      item
        Name = 'Csn2RAp'
        DataType = ftFloat
      end
      item
        Name = 'Csn4Rb'
        DataType = ftFloat
      end
      item
        Name = 'Csn4Rbp'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 41
    Top = 357
    object kbmEsnNo: TIntegerField
      DisplayWidth = 5
      FieldName = 'No'
    end
    object kbmEsnCurTime: TDateTimeField
      DisplayWidth = 14
      FieldName = 'CurTime'
    end
    object kbmEsnEsn: TFloatField
      DisplayWidth = 8
      FieldName = 'Esn'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn21: TFloatField
      FieldName = 'Csn21'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn21p: TFloatField
      FieldName = 'Csn21p'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnEsn21: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Esn21'
      DisplayFormat = '#######0.0000'
      Precision = 4
      Calculated = True
    end
    object kbmEsnCsn38: TFloatField
      FieldName = 'Csn38'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn38p: TFloatField
      FieldName = 'Csn38p'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnEsn38: TFloatField
      FieldName = 'Esn38'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn52: TFloatField
      FieldName = 'Csn52'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn52p: TFloatField
      FieldName = 'Csn52p'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnEsn52: TFloatField
      FieldName = 'Esn52'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn74: TFloatField
      FieldName = 'Csn74'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn74p: TFloatField
      FieldName = 'Csn74p'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnEsn74: TFloatField
      FieldName = 'Esn74'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn2Ra: TFloatField
      FieldName = 'Csn2Ra'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn2Rap: TFloatField
      FieldName = 'Csn2Rap'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnEsn2Ra: TFloatField
      FieldName = 'Esn2Ra'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn4Rb: TFloatField
      FieldName = 'Csn4Rb'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnCsn4Rbp: TFloatField
      FieldName = 'Csn4Rbp'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
    object kbmEsnEsn4Rb: TFloatField
      FieldName = 'Esn4Rb'
      DisplayFormat = '#######0.0000'
      Precision = 4
    end
  end
  object dsEsn: TDataSource
    DataSet = kbmEsn
    Left = 121
    Top = 357
  end
  object ChanelValsQuery: TZQuery
    Connection = elsch_connection
    Params = <>
    Left = 228
    Top = 208
  end
  object dsEvQuery: TDataSource
    DataSet = ChanelValsQuery
    Left = 284
    Top = 208
  end
  object waterarc_conn: TZConnection
    Protocol = 'postgresql-8'
    HostName = '192.168.254.125'
    Database = 'water_arch'
    User = 'scaner'
    Password = 'WinMainQ45'
    Properties.Strings = (
      'codepage=WIN1251'
      'client_encoding=WIN1251')
    Connected = True
    Left = 416
    Top = 24
  end
  object SeverUgArc: TZTable
    Connection = waterarc_conn
    SortedFields = 'date_arc'
    SortType = stDescending
    ReadOnly = True
    TableName = 'public.sever_ug_arc'
    IndexFieldNames = 'date_arc Desc'
    Left = 504
    Top = 24
    object SeverUgArcdate_arc: TDateTimeField
      FieldName = 'date_arc'
    end
    object SeverUgArctotal_heat_used: TFloatField
      FieldName = 'total_heat_used'
    end
    object SeverUgArcheat_for_pipe1: TFloatField
      FieldName = 'heat_for_pipe1'
    end
    object SeverUgArcheat_for_pipe2: TFloatField
      FieldName = 'heat_for_pipe2'
    end
    object SeverUgArcheat_for_pipe3: TFloatField
      FieldName = 'heat_for_pipe3'
    end
    object SeverUgArcvolume_for_pipe1: TFloatField
      FieldName = 'volume_for_pipe1'
    end
    object SeverUgArcvolume_for_pipe2: TFloatField
      FieldName = 'volume_for_pipe2'
    end
    object SeverUgArcvolume_for_pipe3: TFloatField
      FieldName = 'volume_for_pipe3'
    end
    object SeverUgArcavg_temperature_for_pipe1: TFloatField
      FieldName = 'avg_temperature_for_pipe1'
    end
    object SeverUgArcavg_temperature_for_pipe2: TFloatField
      FieldName = 'avg_temperature_for_pipe2'
    end
    object SeverUgArcavg_temperature_for_pipe3: TFloatField
      FieldName = 'avg_temperature_for_pipe3'
    end
    object SeverUgArcavg_pressure_for_pipe1: TFloatField
      FieldName = 'avg_pressure_for_pipe1'
    end
    object SeverUgArcavg_pressure_for_pipe2: TFloatField
      FieldName = 'avg_pressure_for_pipe2'
    end
    object SeverUgArcavg_pressure_for_pipe3: TFloatField
      FieldName = 'avg_pressure_for_pipe3'
    end
    object SeverUgArccold_water_temperature: TFloatField
      FieldName = 'cold_water_temperature'
    end
    object SeverUgArcheat_for_pipe4: TFloatField
      FieldName = 'heat_for_pipe4'
    end
    object SeverUgArcheat_for_pipe5: TFloatField
      FieldName = 'heat_for_pipe5'
    end
    object SeverUgArcheat_for_pipe6: TFloatField
      FieldName = 'heat_for_pipe6'
    end
    object SeverUgArcvolume_for_pipe4: TFloatField
      FieldName = 'volume_for_pipe4'
    end
    object SeverUgArcvolume_for_pipe5: TFloatField
      FieldName = 'volume_for_pipe5'
    end
    object SeverUgArcvolume_for_pipe6: TFloatField
      FieldName = 'volume_for_pipe6'
    end
    object SeverUgArcavg_temperature_for_pipe4: TFloatField
      FieldName = 'avg_temperature_for_pipe4'
    end
    object SeverUgArcavg_temperature_for_pipe5: TFloatField
      FieldName = 'avg_temperature_for_pipe5'
    end
    object SeverUgArcavg_temperature_for_pipe6: TFloatField
      FieldName = 'avg_temperature_for_pipe6'
    end
    object SeverUgArcavg_pressure_for_pipe4: TFloatField
      FieldName = 'avg_pressure_for_pipe4'
    end
    object SeverUgArcavg_pressure_for_pipe5: TFloatField
      FieldName = 'avg_pressure_for_pipe5'
    end
    object SeverUgArcavg_pressure_for_pipe6: TFloatField
      FieldName = 'avg_pressure_for_pipe6'
    end
    object SeverUgArcpk_sua: TIntegerField
      FieldName = 'pk_sua'
      Required = True
    end
    object SeverUgArcarc_code: TIntegerField
      FieldName = 'arc_code'
    end
  end
  object dsSeverUgArc: TDataSource
    DataSet = SeverUgArc
    Left = 564
    Top = 24
  end
  object kbmQot: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'No'
        DataType = ftInteger
      end
      item
        Name = 'CurTime'
        DataType = ftDateTime
      end
      item
        Name = 'Esn'
        DataType = ftFloat
      end
      item
        Name = 'Csn21'
        DataType = ftFloat
      end
      item
        Name = 'Csn21p'
        DataType = ftFloat
      end
      item
        Name = 'Csn38'
        DataType = ftFloat
      end
      item
        Name = 'Csn38p'
        DataType = ftFloat
      end
      item
        Name = 'Csn52'
        DataType = ftFloat
      end
      item
        Name = 'Csn52p'
        DataType = ftFloat
      end
      item
        Name = 'Csn74'
        DataType = ftFloat
      end
      item
        Name = 'Csn74p'
        DataType = ftFloat
      end
      item
        Name = 'Csn2RA'
        DataType = ftFloat
      end
      item
        Name = 'Csn2RAp'
        DataType = ftFloat
      end
      item
        Name = 'Csn4Rb'
        DataType = ftFloat
      end
      item
        Name = 'Csn4Rbp'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 37
    Top = 437
    object kbmQotNo: TIntegerField
      FieldName = 'No'
    end
    object kbmQotDateArc: TDateTimeField
      FieldName = 'DateArc'
    end
    object kbmQotQot: TFloatField
      FieldName = 'Qot'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotQsev: TFloatField
      FieldName = 'Qsev'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotQug: TFloatField
      FieldName = 'Qug'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotGprSev: TFloatField
      FieldName = 'GprSev'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotGobSev: TFloatField
      FieldName = 'GobSev'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotGprUg: TFloatField
      FieldName = 'GprUg'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotGobUg: TFloatField
      FieldName = 'GobUg'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotThol: TFloatField
      FieldName = 'Thol'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotTprSev: TFloatField
      FieldName = 'TprSev'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotTobSev: TFloatField
      FieldName = 'TobSev'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotTprUg: TFloatField
      FieldName = 'TprUg'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
    object kbmQotTobUg: TFloatField
      FieldName = 'TobUg'
      DisplayFormat = '#####0.0000'
      Precision = 4
    end
  end
  object dsQot: TDataSource
    DataSet = kbmQot
    Left = 117
    Top = 437
  end
  object QotQuery: TZQuery
    Connection = waterarc_conn
    Params = <>
    Left = 504
    Top = 96
  end
  object dsQotQuery: TDataSource
    DataSet = QotQuery
    Left = 564
    Top = 96
  end
  object kbmGas: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'No'
        DataType = ftInteger
      end
      item
        Name = 'CurTime'
        DataType = ftDateTime
      end
      item
        Name = 'Esn'
        DataType = ftFloat
      end
      item
        Name = 'Csn21'
        DataType = ftFloat
      end
      item
        Name = 'Csn21p'
        DataType = ftFloat
      end
      item
        Name = 'Csn38'
        DataType = ftFloat
      end
      item
        Name = 'Csn38p'
        DataType = ftFloat
      end
      item
        Name = 'Csn52'
        DataType = ftFloat
      end
      item
        Name = 'Csn52p'
        DataType = ftFloat
      end
      item
        Name = 'Csn74'
        DataType = ftFloat
      end
      item
        Name = 'Csn74p'
        DataType = ftFloat
      end
      item
        Name = 'Csn2RA'
        DataType = ftFloat
      end
      item
        Name = 'Csn2RAp'
        DataType = ftFloat
      end
      item
        Name = 'Csn4Rb'
        DataType = ftFloat
      end
      item
        Name = 'Csn4Rbp'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 37
    Top = 561
    object kbmGasNo: TIntegerField
      FieldName = 'no'
    end
    object kbmGasDateCalc: TDateTimeField
      FieldName = 'datecalc'
    end
    object kbmGasGg: TFloatField
      FieldName = 'Gg'
      DisplayFormat = '########.00'
      Precision = 2
    end
  end
  object dsGas: TDataSource
    DataSet = kbmGas
    Left = 117
    Top = 561
  end
  object GasQuery: TZQuery
    Connection = GSSConnection
    SQL.Strings = (
      'select * from params'
      'where (date_query >= :first_date) and ( date_query < :last_date)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'first_date'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'last_date'
        ParamType = ptUnknown
      end>
    Left = 496
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'first_date'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'last_date'
        ParamType = ptUnknown
      end>
    object GasQueryid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object GasQuerydate_query: TDateTimeField
      FieldName = 'date_query'
    end
    object GasQueryvalue: TFloatField
      FieldName = 'value'
    end
  end
  object kbmAvgFuelCons: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'no'
        DataType = ftInteger
      end
      item
        Name = 'datecalc'
        DataType = ftDateTime
      end
      item
        Name = 'bee'
        DataType = ftFloat
      end
      item
        Name = 'Gg'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 217
    Top = 417
    object kbmAvgFuelConsNo: TIntegerField
      FieldName = 'no'
    end
    object kbmAvgFuelConsDateCalc: TDateTimeField
      FieldName = 'datecalc'
    end
    object kbmAvgFuelConsbee: TFloatField
      FieldName = 'bee'
      DisplayFormat = '########.00'
    end
    object kbmAvgFuelConsGg: TFloatField
      FieldName = 'Gg'
      DisplayFormat = '########.00'
      Precision = 2
    end
    object kbmAvgFuelConsqot: TFloatField
      FieldName = 'qot'
      DisplayFormat = '########.00'
      Precision = 2
    end
    object kbmAvgFuelConsev: TFloatField
      FieldName = 'ev'
      DisplayFormat = '########.00'
      Precision = 2
    end
    object kbmAvgFuelConsesn: TFloatField
      FieldName = 'esn'
      DisplayFormat = '########.00'
      Precision = 2
    end
  end
  object dsAvgFuelCons: TDataSource
    DataSet = kbmAvgFuelCons
    Left = 297
    Top = 417
  end
end
