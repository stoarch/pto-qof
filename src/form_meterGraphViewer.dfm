object MeterGraphViewerForm: TMeterGraphViewerForm
  Left = 322
  Top = 208
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1043#1088#1072#1092#1080#1082' '#1079#1085#1072#1095#1077#1085#1080#1081' '#1089#1095#1105#1090#1095#1080#1082#1072
  ClientHeight = 369
  ClientWidth = 674
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    674
    369)
  PixelsPerInch = 96
  TextHeight = 13
  object CaptionLabel: TsLabel
    Left = 12
    Top = 8
    Width = 114
    Height = 24
    Caption = #1057#1095#1105#1090#1095#1080#1082': '#1093#1093#1093
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 2171169
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object dateRangeLabel: TsLabel
    Left = 12
    Top = 36
    Width = 162
    Height = 13
    Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1079#1072' '#1087#1077#1088#1080#1086#1076' '#1089' '#1093#1093#1093' '#1087#1086' '#1093#1093#1093
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 2171169
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object chtGraph: TChart
    Left = 8
    Top = 60
    Width = 365
    Height = 305
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      #1043#1088#1072#1092#1080#1082' '#1079#1085#1072#1095#1077#1085#1080#1081' '#1089#1095#1105#1090#1095#1080#1082#1072)
    Legend.Visible = False
    View3D = False
    View3DWalls = False
    BevelOuter = bvNone
    BorderWidth = 1
    Color = 16777088
    TabOrder = 0
    Anchors = [akLeft, akTop, akRight, akBottom]
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object dbgrd1: TDBGrid
    Left = 380
    Top = 60
    Width = 289
    Height = 305
    Anchors = [akTop, akRight, akBottom]
    DataSource = dsGraphData
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object sSkinProvider1: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 316
    Top = 252
  end
  object kbmGraphData: TkbmMemTable
    Active = True
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'id'
        DataType = ftInteger
      end
      item
        Name = 'DateQuery'
        DataType = ftDateTime
      end
      item
        Name = 'Value'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 328
    Top = 184
    object kbmGraphDataid: TIntegerField
      FieldName = 'id'
    end
    object kbmGraphDataDateQuery: TDateTimeField
      FieldName = 'DateQuery'
    end
    object kbmGraphDataValue: TFloatField
      FieldName = 'Value'
    end
  end
  object dsGraphData: TDataSource
    DataSet = kbmGraphData
    Left = 328
    Top = 156
  end
end
