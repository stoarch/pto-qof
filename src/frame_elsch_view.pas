unit frame_elsch_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, sLabel, ExtCtrls, DBCtrls,
  sFrameAdapter, DB, kbmMemTable, sButton, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, ImgList, ComCtrls, ToolWin, sToolBar,
  meter_value_list ;

type
  TElschViewFrame = class(TFrame)
    sFrameAdapter1: TsFrameAdapter;
    sLabel1: TsLabel;
    dbgrd1: TDBGrid;
    kbmSelectedCounter: TkbmMemTable;
    dsSelectedCounter: TDataSource;
    kbmSelectedCountercounter_id: TIntegerField;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    dbnvgr2: TDBNavigator;
    sLabel4: TsLabel;
    dbnvgr1: TDBNavigator;
    dbgrd2: TDBGrid;
    dbnvgr3: TDBNavigator;
    dbgrd3: TDBGrid;
    StartPeriodDateEdit: TsDateEdit;
    FilterByDateButton: TsButton;
    EndPeriodDateEdit: TsDateEdit;
    sToolBar1: TsToolBar;
    btnShowGraphButton: TToolButton;
    ilFunctions: TImageList;
    procedure FilterByDateButtonClick(Sender: TObject);
    procedure btnShowGraphButtonClick(Sender: TObject);
  private
    procedure ShowCurrentMeterGraph;
    procedure LoadMeterValues(const values: TMeterValueList);
    procedure ShowMeterGraph(const values: TMeterValueList);
    { Private declarations }
  public
    { Public declarations }
    procedure Execute;
  end;

implementation

{$R *.dfm}

uses
  datamodule_main, factory_Meter, form_meterGraphViewer;
{ TElschViewFrame }

procedure TElschViewFrame.Execute;
begin
    StartPeriodDateEdit.Date := Now - 7;
    EndPeriodDateEdit.Date := Now;
    DataModuleMain.OpenElectroschemeData();
end;

procedure TElschViewFrame.FilterByDateButtonClick(Sender: TObject);
begin
    Cursor := crHourglass;
    Application.ProcessMessages;
    try
      DataModuleMain.SetDatePeriod(
          StartPeriodDateEdit.Date,
          EndPeriodDateEdit.Date
      );
    finally
      Cursor := crDefault;
    end;
end;

procedure TElschViewFrame.btnShowGraphButtonClick(Sender: TObject);
begin
    ShowCurrentMeterGraph();
end;

procedure TElschViewFrame.ShowCurrentMeterGraph();
    var
      meterValues : TMeterValueList;
begin
  meterValues := MeterFactory.MakeValueList();

  try
    LoadMeterValues( meterValues );
    ShowMeterGraph( meterValues );
  finally
    FreeAndNil( meterValues );
  end;
end;

// Load the meter values for currently selected period
//
procedure TElschViewFrame.LoadMeterValues( const values: TMeterValueList );
begin
  DataModuleMain.FillMeterValues( values );
end;

procedure TElschViewFrame.ShowMeterGraph( const values: TMeterValueList );
    var
      GraphViewer: TMeterGraphViewerForm;
begin
    GraphViewer := MeterFactory.MakeGraphViewer( self );

    //we do not free form, because it modeless. All instances freed when this
    //  form is destroyed (by Parent model)

    GraphViewer.SetCaption( DataModuleMain.GetCurrentMeterCaption() + ' - ����� ' + DataModuleMain.GetCurrentChanelCaption() );

    GraphViewer.SetDateRange(
          StartPeriodDateEdit.Date,
          EndPeriodDateEdit.Date
    );
    
    GraphViewer.SetData( values );

    GraphViewer.ShOw();

end;

end.
