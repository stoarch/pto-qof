object GSSViewerFrame: TGSSViewerFrame
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  DesignSize = (
    320
    240)
  object sLabel1: TsLabel
    Left = 4
    Top = 4
    Width = 74
    Height = 13
    Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1043#1056#1057':'
  end
  object dbnvgr1: TDBNavigator
    Left = 4
    Top = 24
    Width = 164
    Height = 25
    DataSource = DataModuleMain.dsParams
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 0
  end
  object dbgrd1: TDBGrid
    Left = 4
    Top = 57
    Width = 312
    Height = 178
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataModuleMain.dsParams
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'GROUPBOX'
    Left = 276
    Top = 16
  end
end
