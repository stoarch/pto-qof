unit form_progressDisplayer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, ComCtrls, acProgressBar, sLabel,
  sSkinProvider, iThreadTimers;

type
  TProgressDisplayerForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    lblWait: TsLabel;
    pbrWorkStatus: TsProgressBar;
    btnCancel: TsButton;
    pbrTaskStatus: TsProgressBar;
    lblSubMessage: TsLabel;
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
    m_cancel 		: boolean;
    m_workMessage   : string;
    FTaskProgress: integer;
    FTaskMax: integer;
    function getWorkMessage: string;
    procedure setWorkMessage(const Value: string);
    function getProgress: Integer;
    procedure setProgress(const Value: Integer);
    function getMaxValue: Integer;
    procedure setMaxValue(const Value: Integer);
    procedure UpdateUI;
    procedure SetTaskMax(const Value: integer);
    procedure SetTaskProgress(const Value: integer);
    function getSubMessage: string;
    procedure setSubMessage(const Value: string);

  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;

    property IsCanceled 	: boolean 	read m_cancel;

    property SubMessage		: string 	read getSubMessage	write setSubMessage;
    property WorkMessage 	: string	read getWorkMessage	write setWorkMessage;

    property TaskProgress	: integer read FTaskProgress write SetTaskProgress;
    property TaskMax		: integer read FTaskMax write SetTaskMax;

    property progress 		: Integer 	read getProgress	write setProgress;
    property MaxValue		: Integer	read getMaxValue 	write setMaxValue;
  end;

var
  ProgressDisplayerForm: TProgressDisplayerForm;

implementation

{$R *.dfm}

procedure TProgressDisplayerForm.btnCancelClick(Sender: TObject);
begin
	m_cancel := True;
end;

constructor TProgressDisplayerForm.Create(AOwner: TComponent);
begin
  inherited;

  m_cancel := false;
  m_workMessage := '��������� ����������...';
end;

function TProgressDisplayerForm.getMaxValue: Integer;
begin
	result := pbrWorkStatus.Max;
end;

function TProgressDisplayerForm.getProgress: Integer;
begin
	result := pbrWorkStatus.Position;
end;

function TProgressDisplayerForm.getSubMessage: string;
begin
	Result	:=	lblSubMessage.Caption;
end;

function TProgressDisplayerForm.getWorkMessage: string;
begin
	result := m_workMessage;
end;

procedure TProgressDisplayerForm.setMaxValue(const Value: Integer);
begin
	pbrWorkStatus.Max := value;

    UpdateUI();
end;

procedure TProgressDisplayerForm.setProgress(const Value: Integer);
begin
	pbrWorkStatus.position := value;

    UpdateUI();
end;

procedure TProgressDisplayerForm.setSubMessage(const Value: string);
begin
	lblSubMessage.Caption	:=	value;

    UpdateUI();
end;

procedure TProgressDisplayerForm.SetTaskMax(const Value: integer);
begin
  FTaskMax := Value;
  pbrTaskStatus.Max := value;

  UpdateUI();
end;

procedure TProgressDisplayerForm.SetTaskProgress(const Value: integer);
begin
  FTaskProgress := Value;
  pbrTaskStatus.Position := value;

  UpdateUI();
end;

procedure TProgressDisplayerForm.setWorkMessage(const Value: string);
begin
	m_workMessage 	:= value;
    lblWait.Caption := value;

    UpdateUI();
end;

procedure TProgressDisplayerForm.UpdateUI();
begin
    Refresh();
    Update();
    Application.ProcessMessages();
end;


end.
