unit frame_gss_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Grids, DBGrids, ExtCtrls, DBCtrls, StdCtrls, sLabel,
  sFrameAdapter, datamodule_main;

type
  TGSSViewerFrame = class(TFrame)
    sFrameAdapter1: TsFrameAdapter;
    sLabel1: TsLabel;
    dbnvgr1: TDBNavigator;
    dbgrd1: TDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute;
  end;

implementation

{$R *.dfm}

{ TGSSViewerFrame }

procedure TGSSViewerFrame.Execute;
begin
    DataModuleMain.OpenGSSData();
end;

end.
